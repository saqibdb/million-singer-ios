//
//  MenuVideoFinalizeViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 25/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
@interface MenuVideoFinalizeViewController : UIViewController<AVPlayerViewControllerDelegate>
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)menueSaveAction:(UIButton *)sender;
- (IBAction)uploadAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *VideoPreviewView;
@property (weak, nonatomic) IBOutlet UIButton *blayBtn;
- (IBAction)playAction:(UIButton *)sender;
@property (strong) NSURL *videoUrl;
@property (nonatomic) AVPlayerViewController *playerViewController;
@end
