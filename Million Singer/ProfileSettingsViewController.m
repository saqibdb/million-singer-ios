//
//  ProfileSettingsViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 17/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ProfileSettingsViewController.h"
#import "ProfileMenueTableViewCell.h"
#import "RechargViewController.h"
#import "MessagesViewController.h"
#import "Localisator.h"
@interface ProfileSettingsViewController (){
    NSArray *fansListArray;
    NSArray *singersListArray;
    NSInteger senderTag;

}

@end

@implementation ProfileSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    senderTag = 0;
    for (UIButton *btn in self.fansAndSingersBtns) {
        if (btn.tag == 0) {
            [btn setTitle:LOCALIZATION(@"Fans") forState:UIControlStateNormal];
        }
        else if(btn.tag == 1)   {
            [btn setTitle:LOCALIZATION(@"singer") forState:UIControlStateNormal];
        }
    }
    self.lblPersonal.text = LOCALIZATION(@"personal");
    self.lblAttentions.text = LOCALIZATION(@"attention");
    self.lblFans.text = LOCALIZATION(@"Fans") ;
    fansListArray = [[NSArray alloc]initWithObjects:LOCALIZATION(@"My items"),LOCALIZATION(@"My wallet"),LOCALIZATION(@"Mall"),LOCALIZATION(@"Service"),LOCALIZATION(@"information"),LOCALIZATION(@"Gift record"),LOCALIZATION(@"Watch History"),LOCALIZATION(@"Other settings"), nil];
   
    
    singersListArray = [[NSArray alloc]initWithObjects:LOCALIZATION(@"My page"),LOCALIZATION(@"All applications"),LOCALIZATION(@"My rankings"),LOCALIZATION(@"my fans"),LOCALIZATION(@"My film"),LOCALIZATION(@"Race"), nil];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (senderTag == 0) {
        return fansListArray.count;
    }
    else
    {
        return singersListArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"menueCell";
    ProfileMenueTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[ProfileMenueTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
    }
    
    if (senderTag == 0) {
        NSString* menueName = [fansListArray objectAtIndex:indexPath.row];
        cell.menuNameLabel.text = menueName;
    }
    else
    {
        NSString* menueName = [singersListArray objectAtIndex:indexPath.row];
        cell.menuNameLabel.text = menueName;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (senderTag == 0) {
        if (indexPath.row == 0) {
            [self performSegueWithIdentifier:@"profileToMyitems" sender:self];
        }
    }
    
    if (senderTag == 0) {
        if (indexPath.row == 1) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"UmerDesigns" bundle:nil];
            RechargViewController *viewController = (RechargViewController *)[storyboard instantiateViewControllerWithIdentifier:@"RechargViewController"];
            [self presentViewController:viewController animated:YES completion:nil];
        }
    }

    if (senderTag == 0) {
        if (indexPath.row == 2) {
            [self performSegueWithIdentifier:@"profileToMAllVc" sender:self];
        }
    }
    if (senderTag == 0) {
        if (indexPath.row == 3) {
            [self performSegueWithIdentifier:@"profileToService" sender:self];
        }
    }
    
    if (senderTag == 0) {
        if (indexPath.row == 4) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"UmerDesigns" bundle:nil];
            MessagesViewController *viewController = (MessagesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MessagesViewController"];
            [self presentViewController:viewController animated:YES completion:nil];
        }
    }
    
    if (senderTag == 0) {
        if (indexPath.row == 5) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"UmerDesigns" bundle:nil];
            MessagesViewController *viewController = (MessagesViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MessagesViewController"];
            [self presentViewController:viewController animated:YES completion:nil];
        }
    }

    if (senderTag == 0) {
        if (indexPath.row == 6) {
            [self performSegueWithIdentifier:@"profileToHistory" sender:self];
        }
    }
    
    if (senderTag == 0) {
        if (indexPath.row == 7) {
            [self performSegueWithIdentifier:@"accountSettingToOtherSetting" sender:self];
        }
    }
    
}

#pragma mark - Actions

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fanAndSingerActions:(UIButton *)sender {
   
    
    //changing btn color on selections
    UIColor *myThemeColor = [UIColor colorWithRed:78/255.0 green:101/255.0 blue:115/255.0 alpha:1];
    for (UIButton *btn in self.fansAndSingersBtns) {
        if (btn.tag == sender.tag) {
            
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else{
            
            [btn setTitleColor:myThemeColor forState:UIControlStateNormal];
        }
    }

    if (sender.tag == 0) {
        senderTag = 0;
    }
    else
    {
        senderTag = 1;
    }
    [self.tableView reloadData];
    
   
}
@end
