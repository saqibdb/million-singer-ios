//
//  CompetitionEventsTableViewCell.m
//  Million Singer
//
//  Created by MACBOOK PRO on 12/09/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "CompetitionEventsTableViewCell.h"

@implementation CompetitionEventsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.attentionBtn.layer setCornerRadius:8];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
