//
//  MyItemsViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyItemsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)positiveAndNegativeAssessmentActions:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *positiveAndNegativeAssessmentBtns;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backAction:(UIButton *)sender;

@end
