//
//  RechargViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "RechargViewController.h"
#import "RechargeTableViewCell.h"
#import "ServiceViewController.h"
#import "Localisator.h"

@interface RechargViewController ()

@end

@implementation RechargViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.getFreeGoldCoinsBtn.layer setCornerRadius:12];
    [self setLocalizedTitles];
    // Do any additional setup after loading the view.
}

-(void)setLocalizedTitles{
    
    self.lblWantCoins.text = LOCALIZATION(@"Want free gold coins?");
    self.lblGoldBalence.text = LOCALIZATION(@"gold_balance");
    self.rechargeTitle.text = LOCALIZATION(@"Recharge");
    [self.btnDoBuisness setTitle:LOCALIZATION(@"Do business") forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"rechargCell";
    RechargeTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[RechargeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:MyIdentifier];
    }
    
    switch (indexPath.row) {
        case 0:
        {
            cell.amountLabel.text = @"50";
            cell.giveAwayAmountLable.text = @"";
            [cell.dollorsBtn setTitle:@"$1 USD" forState:UIControlStateNormal];
            break;
        }
        case 1:
        {
            cell.amountLabel.text = @"150";
            cell.giveAwayAmountLable.text = @"送 150";
            [cell.dollorsBtn setTitle:@"$3 USD" forState:UIControlStateNormal];
            break;
        }
        case 2:
        {
            cell.amountLabel.text = @"400";
            cell.giveAwayAmountLable.text = @"送 280";
            [cell.dollorsBtn setTitle:@"$8 USD" forState:UIControlStateNormal];
            break;
        }
        case 3:
        {
            cell.amountLabel.text = @"1,000";
            cell.giveAwayAmountLable.text = @"送 120";
            [cell.dollorsBtn setTitle:@"$12 USD" forState:UIControlStateNormal];
            break;
        }
        case 4:
        {
            cell.amountLabel.text = @"2,500";
            cell.giveAwayAmountLable.text = @"送 250";
            [cell.dollorsBtn setTitle:@"$57 USD" forState:UIControlStateNormal];
            break;
        }
        case 5:
        {
            cell.amountLabel.text = @"4,500";
            cell.giveAwayAmountLable.text = @"送 1,800";
            [cell.dollorsBtn setTitle:@"$98 USD" forState:UIControlStateNormal];
            break;
        }
        default:
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPat{
    
}

#pragma mark - Actions

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)getFreeGoldCoinsAction:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AccountSettings" bundle:nil];
    ServiceViewController *viewController = (ServiceViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ServiceViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}
@end
