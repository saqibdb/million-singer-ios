//
//  MenuVideoEditViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 22/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "MenuVideoEditViewController.h"
#import "GPUImage.h"
#import "SVProgressHUD.h"
#import "MenuVideoFinalizeViewController.h"

@interface MenuVideoEditViewController ()
{
    SAVideoRangeSlider *mySAVideoRangeSlider;
    SCFilter *exportFilter;
     NSURL *urlToSend;
    NSURL *orignalVideoURL;
    NSURL *orignalVideoURLCopy;
    NSURL *videoWithFilterToTrimUrl;
    NSArray *effectImageNames;
    NSInteger videoDurationSeconds;
    bool trimOrignalVideo;
    AVAudioEngine *engine;
    AVAudioPlayerNode* node;
    AVPlayerItem* playerItem ;
}
@end

@implementation MenuVideoEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self settingLayouts];
    
    effectImageNames = [[NSArray alloc]initWithObjects:@"effectSample",@"effectSample_green",@"effectSample_red",@"effectSample_blue",@"effectSample_normal", nil];
    [self addmovieWithFilter:self.videoUrl];
    orignalVideoURL = self.videoUrl;
    orignalVideoURLCopy = self.videoUrl;
    urlToSend = self.videoUrl;
    // Do any additional setup after loading the view.
    //b&w green red blu , normal
}

-(void)settingLayouts{
    [self.view layoutIfNeeded];
    self.effectsCollectionView.dataSource = self;
    self.effectsCollectionView.delegate = self;
    [self.cropView setHidden:YES];
    [self.slideView setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([segue.identifier isEqualToString:@"editToComplitonVC"]) {
         MenuVideoFinalizeViewController *VC = segue.destinationViewController;
         VC.videoUrl = urlToSend;
     }
 }

#pragma mark - collection Delegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 5;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    UICollectionViewCell *cell=(UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"effectsCollectionView" forIndexPath:indexPath];
    
    NSString *imageName = [effectImageNames objectAtIndex:indexPath.row];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    [imageView setImage:[UIImage imageNamed:imageName]];
    
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (orignalVideoURL) {
        
    
    if ([collectionView isEqual:self.effectsCollectionView]) {
        switch (indexPath.row) {
            case 0:
            {
                exportFilter = [SCFilter filterWithCIFilterName:@"CIPhotoEffectTonal"];
                break;
            }
            case 1:
            {
                exportFilter = [SCFilter filterWithCIFilterName:@"CIPhotoEffectProcess"];
                break;
            }
            case 2:
            {
                exportFilter = [SCFilter filterWithCIFilterName:@"CIPhotoEffectInstant"];
                break;
            }
            case 3:
            {
                exportFilter = [SCFilter filterWithCIFilterName:@"CIPhotoEffectFade"];
                break;
            }
            case 4:
            {
                exportFilter = [SCFilter filterWithCIFilterName:@"CIPhotoEffectChrome"];
                break;
            }
                
            default:
                break;
        }
        
        [self applyFilter:orignalVideoURL];
        }
     
    }
}

- (IBAction)closeAction:(UIButton *)sender {
    if (self.navigationController != nil) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}


- (IBAction)nextAction:(UIButton *)sender {
}
- (IBAction)cropDoneAction:(UIButton *)sender {
    [self.cropView setHidden:YES];
    [self.slideView setHidden:NO];
}

- (IBAction)showCropOptionsAction:(UIButton *)sender {
    [self.cropView setHidden:NO];
    [self.slideView setHidden:YES];
}

- (IBAction)playAction:(UIButton *)sender {
    if (sender.tag == 0) {
        sender.tag = 1;
        
       
        [node play];
        [self.playerViewController.player play];
    }
    else
    {
        sender.tag = 0;
        [node stop];
        [self.playerViewController.player setMuted:NO];
        [self.playerViewController.player pause];
        [playerItem seekToTime:kCMTimeZero];
    }
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)applyFilter:(NSURL*)videoUrl{
     [node stop];
    [SVProgressHUD showWithStatus:@"Applying filter..."];
    NSString *fileName = videoUrl.lastPathComponent;
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    
    AVAsset *asset = [AVAsset assetWithURL:videoUrl];
    AVAssetTrack *track     = [[asset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    
    AVMutableComposition *composition = [AVMutableComposition composition];
    [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.renderSize = track.naturalSize;
    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);
    
    AVMutableVideoCompositionLayerInstruction *transformer = [AVMutableVideoCompositionLayerInstruction
                                                              videoCompositionLayerInstructionWithAssetTrack:track];
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject:instruction];
    
    SCAssetExportSession *exportSession = [[SCAssetExportSession alloc] initWithAsset:asset];
    
    NSURL *urlFile = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@.mov",docsDir,fileName]];
    exportSession.outputUrl = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@",urlFile]];
    NSURL* filterURL = exportSession.outputUrl;
    
    exportSession.videoConfiguration.composition = videoComposition;
    exportSession.videoConfiguration.filter = exportFilter;
    exportSession.videoConfiguration.preset = SCPresetHighestQuality;
    exportSession.audioConfiguration.preset = SCPresetHighestQuality;
    exportSession.videoConfiguration.maxFrameRate = 35;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.delegate = self;
    exportSession.contextType = SCContextTypeAuto;
    
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if (exportSession.error == nil) {
            
            [self.playerViewController.view removeFromSuperview];
            self.playerViewController = nil;
            [self addmovieWithFilter:filterURL];
            videoWithFilterToTrimUrl = filterURL;
             urlToSend = filterURL;
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            NSLog(@"Error: %@", exportSession.error);
        }
    }];
}

-(void)addmovieWithFilter:(NSURL *)url{
    
    if (!_playerViewController) {
        
        _playerViewController = [[AVPlayerViewController alloc] init];
        _playerViewController.delegate = self;
        _playerViewController.view.frame = CGRectMake(0, 0, self.videoPreviewView.frame.size.width, self.videoPreviewView.frame.size.height) ;
        _playerViewController.showsPlaybackControls = NO;
        // First create an AVPlayerItem
         playerItem = [AVPlayerItem playerItemWithURL:url];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
        _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
        [self.videoPreviewView addSubview:_playerViewController.view];
        [mySAVideoRangeSlider removeFromSuperview];
        [self settingUpVideoTrimmer:url];
    }
}

-(void)settingUpVideoTrimmer:(NSURL *)videoFileUrl{
    mySAVideoRangeSlider = [[SAVideoRangeSlider alloc] initWithFrame:self.trimViewContainer.bounds videoUrl:videoFileUrl ];
    [mySAVideoRangeSlider setTintColor:[UIColor redColor]];
    [mySAVideoRangeSlider setPopoverBubbleSize:200 height:100];
    mySAVideoRangeSlider.delegate = self;
    mySAVideoRangeSlider.bubleText.font = [UIFont systemFontOfSize:12];
    [mySAVideoRangeSlider setPopoverBubbleSize:120 height:60];
    [self.trimViewContainer addSubview:mySAVideoRangeSlider];
}

- (void)videoRange:(SAVideoRangeSlider *)videoRange didChangeLeftPosition:(CGFloat)leftPosition rightPosition:(CGFloat)rightPosition{
    CMTime targetTime = CMTimeMakeWithSeconds(leftPosition, NSEC_PER_SEC);
    [[[_playerViewController player] currentItem] seekToTime:targetTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
}

-(void)trimVideoWithURL:(NSURL *)url
{
    if (trimOrignalVideo) {
         [SVProgressHUD showWithStatus:@"Processing video..."];
    }
   
    AVURLAsset *anAsset = [[AVURLAsset alloc] initWithURL:url options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:anAsset];
    if ([compatiblePresets containsObject:AVAssetExportPresetMediumQuality]) {
        
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]
                                               initWithAsset:anAsset presetName:AVAssetExportPresetHighestQuality];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *myPathDocs;
        
        myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                       [NSString stringWithFormat:@"FinalVideoEdtor-%d.mov", arc4random() % 1000]];
        
        NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
        if ([[NSFileManager defaultManager] fileExistsAtPath:myPathDocs])
            [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error:nil];
        
        exportSession.outputURL = outputurl;
        exportSession.outputFileType = AVFileTypeQuickTimeMovie;
        
        CMTime start = CMTimeMakeWithSeconds(mySAVideoRangeSlider.leftPosition, anAsset.duration.timescale);
        CMTime duration = CMTimeMakeWithSeconds(mySAVideoRangeSlider.rightPosition-mySAVideoRangeSlider.leftPosition, anAsset.duration.timescale);
        CMTimeRange range = CMTimeRangeMake(start, duration);
        exportSession.timeRange = range;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                    
    
                    [SVProgressHUD dismiss];
                    if (trimOrignalVideo) {
                        trimOrignalVideo = NO;
                        [self trimVideoWithURL:orignalVideoURL];
                        [self.playerViewController.view removeFromSuperview];
                        self.playerViewController = nil;
                        [self addmovieWithFilter:outputurl];
                        videoWithFilterToTrimUrl = outputurl;
                    }
                    else{
                        orignalVideoURL = outputurl;
                         urlToSend = outputurl;
                    }
                }
                else if (exportSession.status == AVAssetExportSessionStatusFailed)
                {
                    [SVProgressHUD dismiss];
                    NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                    
                    
                } else if (exportSession.status == AVAssetExportSessionStatusCancelled)
                {
                    [SVProgressHUD dismiss];
                    NSLog(@"Export canceled");
                    
                }
            });
        }];
    }
}


-(NSString*) applicationDocumentsDirectory
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (IBAction)applyFiltersAction:(UIButton *)sender {
     [node stop];
    trimOrignalVideo = YES;
    if (videoWithFilterToTrimUrl) {
        [self trimVideoWithURL:videoWithFilterToTrimUrl];
    }
}

- (IBAction)clearAllFiltersAction:(UIButton *)sender {
    if (orignalVideoURLCopy) {
        [self.playerViewController.view removeFromSuperview];
        self.playerViewController = nil;
        [self addmovieWithFilter:orignalVideoURLCopy];
        orignalVideoURL = orignalVideoURLCopy;
        [SVProgressHUD dismiss];
    }
}

-(void)getAudioFromVideo{
}

-(void)changeAudioNode:(NSURL*)url{
    
    engine = [[AVAudioEngine alloc] init];
//    NSString* path=[[NSBundle mainBundle] pathForResource:@"cheapThrills" ofType:@"mp3"];
//    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    AVAudioFile* File = [[AVAudioFile alloc] initForReading: url error: nil];
    node = [AVAudioPlayerNode new];
    
    [node stop];
    [engine stop];
    [engine reset];
    [engine attachNode: node];
    AVAudioUnitTimePitch* changeAudioUnitTime = [AVAudioUnitTimePitch new];
    
    changeAudioUnitTime.rate = 1;
    changeAudioUnitTime.pitch = 600;
    [engine attachNode: changeAudioUnitTime];
    [engine connect:node to: changeAudioUnitTime format: nil];
    [engine connect:changeAudioUnitTime to: engine.outputNode format:nil];
    [node scheduleFile:File atTime: nil completionHandler: nil];
    [engine startAndReturnError: nil];
    [node play];
    [playerItem seekToTime:kCMTimeZero];
    [self.playerViewController.player play];
    [self.playerViewController.player setMuted:YES];
    self.playBtn.tag = 1;

}

- (IBAction)applyAudioFilter:(UIButton *)sender {
    [self convertVideoToAudioWithInputURL:urlToSend];
}

- (void)convertVideoToAudioWithInputURL:(NSURL*)inputURL
{
    AVURLAsset* asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset
                                                          presetName: AVAssetExportPresetPassthrough];
   
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs;
    
    myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                   [NSString stringWithFormat:@"audioEdtor-%d.m4a", arc4random() % 1000]];
    
    NSURL *outputurl = [NSURL fileURLWithPath:myPathDocs];
    
    exportSession.outputURL = outputurl;
    exportSession.outputFileType = AVFileTypeAppleM4A; //For audio file
    exportSession.timeRange = CMTimeRangeMake(kCMTimeZero, [asset duration]);
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                
                [SVProgressHUD dismiss];
                [self changeAudioNode:exportSession.outputURL];
            }
            else if (exportSession.status == AVAssetExportSessionStatusFailed)
            {
                [SVProgressHUD dismiss];
                NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
             }
            else if (exportSession.status == AVAssetExportSessionStatusCancelled)
            {
                [SVProgressHUD dismiss];
                NSLog(@"Export canceled");
            }
        });
     }];
}




@end
