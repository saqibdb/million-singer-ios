//
//  FollowersViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 20/09/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "FollowersViewController.h"
#import "RankTableViewCell.h"
@interface FollowersViewController ()

@end

@implementation FollowersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - tableViewDelegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0  ) {
        return 110;
    }
    else
    {
        return  75;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"rankCell";
    RankTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[RankTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:MyIdentifier];
    }
    
    if (indexPath.row == 0)
    {
        [cell.rankTypeImageView setImage:[UIImage imageNamed:@"rankYellow"]];
        cell.cellHeightConstraint.constant = cell.contentView.frame.size.height;
    }
    else if (indexPath.row == 1){
        [cell.rankTypeImageView setImage:[UIImage imageNamed:@"rankWhite"]];
        cell.cellHeightConstraint.constant = 65;
        
    }
    else if (indexPath.row == 2){
        [cell.rankTypeImageView setImage:[UIImage imageNamed:@"rankBrown"]];
        cell.cellHeightConstraint.constant = 65;
        
    }
    else{
        
    }
    cell.secondUserName.text = @"觀眾： 1,123 分";
        cell.totalFansCount.text =  @"觀眾： 1,123 分";
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"rankToMyPage" sender:self];
}



- (IBAction)menuActions:(UIButton *)sender {
}

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
