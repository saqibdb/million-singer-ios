//
//  ResponseViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 22/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResponseViewController : UIViewController
- (IBAction)backAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *responseBtns;


@end
