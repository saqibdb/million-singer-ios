//
//  FollowersViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 20/09/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)menuActions:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;
@end
