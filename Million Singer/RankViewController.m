//
//  RankViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 12/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "RankViewController.h"
#import "Localisator.h"
@interface RankViewController ()
{
    NSInteger listSelectionTag;
}

@end

@implementation RankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    listSelectionTag = 0;
    self.RankTableView.dataSource = self;
    self.RankTableView.delegate = self;
    [self setButtonTitles];
    
    self.RankTableView.rowHeight = UITableViewAutomaticDimension;
    self.RankTableView.estimatedRowHeight = 75;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setButtonTitles{
    //list
    for (UIButton *btn in self.listSelectionButtons) {
        if (btn.tag == 0) {
            [btn setTitle:LOCALIZATION(@"popularity_list") forState:UIControlStateNormal];
        }
        else if (btn.tag == 1) {
            [btn setTitle:LOCALIZATION(@"lesson_list") forState:UIControlStateNormal];
        }
        else if (btn.tag == 2) {
            [btn setTitle:LOCALIZATION(@"star_list") forState:UIControlStateNormal];
        }
        else if (btn.tag == 3) {
            [btn setTitle:LOCALIZATION(@"Millions_of_bonus_towers") forState:UIControlStateNormal];
        }
       
    }
    
    //sublist
    for (UIButton *btn in self.subListSelectionButtons) {
        if (btn.tag == 0) {
            [btn setTitle:LOCALIZATION(@"day_chart") forState:UIControlStateNormal];
        }
        else if (btn.tag == 1) {
            [btn setTitle:LOCALIZATION(@"monthly_list") forState:UIControlStateNormal];
        }
        else if (btn.tag == 2) {
            [btn setTitle:LOCALIZATION(@"quarter_list") forState:UIControlStateNormal];
        }
        else if (btn.tag == 3) {
            [btn setTitle:LOCALIZATION(@"general_list") forState:UIControlStateNormal];
        }
        
    }
    
    //global and area buttons
    
    for (UIButton *btn in self.globalAndAreaButtons) {
        if (btn.tag == 0) {
            [btn setTitle:LOCALIZATION(@"global") forState:UIControlStateNormal];
        }
        else if (btn.tag == 1) {
            [btn setTitle:LOCALIZATION(@"area") forState:UIControlStateNormal];
        }
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tableViewDelegates
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row == 0  ) {
//        return 110;
//    }
//    else
//    {
//        return  75;
//    }
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RankTableViewCell  *cell;
    
    if (indexPath.row == 0)
    {
        static NSString *MyIdentifier = @"rankCell1";

        cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];

        if (cell == nil)
        {
            
            cell = [[RankTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
        }
    }
    else
    {
        static NSString *MyIdentifier = @"rankCell2";
        
        cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
        
        if (cell == nil)
        {
            
            cell = [[RankTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
        }
    }
    
    
    
    if (indexPath.row == 0)
    {
        [cell.rankTypeImageView setImage:[UIImage imageNamed:@"rankYellow"]];
        cell.lblRank.hidden = TRUE;
        cell.rankTypeImageView.hidden = FALSE;

    }
    else if (indexPath.row == 1)
    {
        [cell.rankTypeImageView setImage:[UIImage imageNamed:@"rankWhite"]];
        cell.lblRank.hidden = TRUE;
        cell.rankTypeImageView.hidden = FALSE;

    
    }
    else if (indexPath.row == 2)
    {
        [cell.rankTypeImageView setImage:[UIImage imageNamed:@"rankBrown"]];
        cell.lblRank.hidden = TRUE;
        cell.rankTypeImageView.hidden = FALSE;

    
    }
    else
    {
        cell.rankTypeImageView.hidden = TRUE;
        
        cell.lblRank.hidden = FALSE;
        cell.lblRank.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];

    }
    
    [cell.attentionBtn setTitle:LOCALIZATION(@"attention") forState:UIControlStateNormal];
    cell.totalFansCount.text =  @"貢獻： 1,000";
    [cell.secondUserName setHidden:YES];
    if (listSelectionTag  == 2) {
        [cell.secondUserName setHidden:NO];
        cell.secondUserName.text = @"觀眾： 1,123 分";
        cell.totalFansCount.text =  @"觀眾： 1,123 分";
    }
    else if(listSelectionTag == 3){
        [self showPopUpUnderConstruction];
    }
    
       return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"rankToMyPage" sender:self];
}

#pragma mark - Actions

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)listSelectionActions:(UIButton *)sender {
    listSelectionTag = sender.tag;
    [self.RankTableView  reloadData];
}

- (IBAction)subListSelectionActions:(UIButton *)sender {
    [self showPopUpUnderConstruction];

}

- (IBAction)globalAndAreaActions:(UIButton *)sender {
    [self showPopUpUnderConstruction];

}

- (IBAction)dateAction:(UIButton *)sender {
    [self showPopUpUnderConstruction];

}
-(void)showPopUpUnderConstruction{

    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"Under Construction"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* OkBtn = [UIAlertAction
                            actionWithTitle:@"Ok"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action) {
                                
                                //Handle your yes please button action here
                            }];
    [alert addAction:OkBtn];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
