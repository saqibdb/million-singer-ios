//
//  MenuVideoFinalizeViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 25/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "MenuVideoFinalizeViewController.h"
#import "UIViewController+CWPopup.h"
#import "UserCoinsPopUpView.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"



@interface MenuVideoFinalizeViewController ()

@end

@implementation MenuVideoFinalizeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.useBlurForPopup = YES;
   
}
-(void)viewDidAppear:(BOOL)animated{
   
    [self setUpVideoForPreview:self.videoUrl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpVideoForPreview:(NSURL *)url{
    
    if (!_playerViewController) {
        
        _playerViewController = [[AVPlayerViewController alloc] init];
        _playerViewController.delegate = self;
        _playerViewController.view.frame = CGRectMake(0, 0, self.VideoPreviewView.frame.size.width, self.VideoPreviewView.frame.size.height) ;
        _playerViewController.showsPlaybackControls = YES;
        // First create an AVPlayerItem
        AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:url];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
        _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
        [self.VideoPreviewView addSubview:_playerViewController.view];
        
      
       
    }
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)openUserCoinsPopup {
    UserCoinsPopUpView *goldCoinsPopUpView = [[UserCoinsPopUpView alloc] initWithNibName:@"UserCoinsPopUpView" bundle:nil];
    [self presentPopupViewController:goldCoinsPopUpView animated:YES completion:^(void) {
        [goldCoinsPopUpView.determinebtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)dismissPopup {
   
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"dissmissed");
        }];
    
}
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)menueSaveAction:(UIButton *)sender {

    NSData *urlData = [NSData dataWithContentsOfURL:self.videoUrl];
    NSString  *documentsDirectory ;
    if ( urlData )
    {
        NSString *docsDir;
        NSArray *dirPaths;
        NSString *filnameToBe = [NSString stringWithFormat:@"%umillionSinger.mov",arc4random() % 100000000];
        filnameToBe = [filnameToBe stringByReplacingOccurrencesOfString:@" " withString:@""];
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:filnameToBe]];
        if ([urlData writeToFile:databasePath atomically:YES]) {
            NSLog(@"Path:%@",databasePath);
            ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
            NSURL *theURL = [NSURL fileURLWithPath:databasePath isDirectory:YES];
            NSError *err;
            if ([theURL checkResourceIsReachableAndReturnError:&err] == NO){
                NSLog(@"failed to retrieve image asset:\nError: %@ ", err);

            }
            else{
                NSLog(@"File Exists");
                [library saveVideo:theURL toAlbum:@"PlayTrip" completion:^(NSURL *assetURL, NSError *error) {
                    [self openUserCoinsPopup];

                    
                } failure:^(NSError *error) {
                    NSLog(@"failed to retrieve image asset:\nError: %@ ", [error localizedDescription]);

                }];
            }
        }
        else{
            NSLog(@"File Not Saved. Please Try Again.");


        }
  
     }
 }

- (IBAction)uploadAction:(UIButton *)sender {
    [self openUserCoinsPopup];

}
- (IBAction)playAction:(UIButton *)sender {
}
@end
