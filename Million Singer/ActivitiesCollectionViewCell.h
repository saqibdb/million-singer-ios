//
//  ActivitiesCollectionViewCell.h
//  Million Singer
//
//  Created by Ali Apple  on 8/9/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivitiesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *mainText;

@end
