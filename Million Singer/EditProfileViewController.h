//
//  EditProfileViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *changeProfileOptionsView;
- (IBAction)changeProfileOptionsCancelAction:(UIButton *)sender;
- (IBAction)cangeProfileAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *shareView;
- (IBAction)canceShareAction:(UIButton *)sender;

- (IBAction)shareAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtNickName;
@property (weak, nonatomic) IBOutlet UITextField *txtIntro;
@property (weak, nonatomic) IBOutlet UITextField *txtGender;
@property (weak, nonatomic) IBOutlet UITextField *txtDOB;
@property (weak, nonatomic) IBOutlet UITextField *txtHoroscope;
@property (weak, nonatomic) IBOutlet UITextField *txtLocation;
- (IBAction)saveProfileAction:(UIButton *)sender;
- (IBAction)takePicORChooseAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;



@end
