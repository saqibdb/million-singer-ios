//
//  MyFilmsViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 25/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFilmsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
