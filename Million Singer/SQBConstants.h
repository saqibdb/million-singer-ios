//
//  SQBConstants.h
//  SQBEmojiSDK
//
//  Created by iBuildx_Mac_Mini on 12/1/16.
//  Copyright © 2016 iBuildX. All rights reserved.
//

#ifndef SQBConstants_h
#define SQBConstants_h

#define kBASE_URL @"http://eb01.com.hk/millionsinger/web/api/"
#define kLoginReg @"userlogin_reg"

#define kVerifyOtp @"verifyotp"
#define kProfileUpdate @"profileupdate"
#define kUserProfileView @"userprofileview"
#define kLogout @"userlogout"
#define kUserImage @"userimage"



#define kSignUp @"signup/"
#define kRegisterPush @"device/pns/register/"
#define kForgetPassword @"password/forget/"
#define kResetPassword @"password/update/"
#define kUserOpportunities @"opportunities/query/"
#define kApplicantInfo @"applicant/query/"
#define kUserProfile @"profile/"
#define kUpdateSummary @"applicant/summary/update/"
#define kAddExperience @"applicant/experience/add/"
#define kUpdateExperience @"applicant/experience/update/"
#define kRemoveExperience @"applicant/experience/remove/"
#define kAddEducation @"applicant/education/add/"
#define kUpdateEducation @"applicant/education/update/"
#define kRemoveEducation @"applicant/education/remove/"

#define kOppurtunityDetail @"/detail/"

#define kSkillsList @"skills/query/"
#define kAttributionList @"attributions/query/"
#define kComments @"comments/query/"
#define kaddComment @"comment/add/"
#define kApply @"apply/"
#define kVote @"vote/"
#define kDetail @"detail/"
#define kCreateOpp @"opportunity/create/"
#define kUpdateOpp @"update/"

#define kEmailLink @"email/link/"
#define kUpdateState @"state/update/"



#define kApplicants @"applicants/query/"

#define kMembership @"membership/"




#define kUserAppliedOpportunities @"opportunities/applied/query/"

//https://www.openworkr.com/m/opportunity/3/detail/

#define kExperienceCellHeight 90
#define kEducationCellHeight 60

#define kLoggedInKey @"LoggedIn_Response"
#define kUserKey @"Saved_User"
#define kUserProfileKey @"Current_User_Profile"
#define kApplicantInfoKey @"Applicant_Info"

#define kDeviceToken @"Device_Token"
#define kPushState @"Push_State"




#endif /* SQBConstants_h */
