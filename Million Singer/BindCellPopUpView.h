//
//  CallUsPopUpView.h
//  Basiligo
//
//  Created by ibuildx on 1/29/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>

@interface BindCellPopUpView : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet CCMBorderView *topBorderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceConstraint;




- (IBAction)shareAction:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIView *_callUsPopUpView;


@end
