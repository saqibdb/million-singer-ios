//
//  MessagesTableViewCell.h
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *messgeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *replyBtn;
@property (weak, nonatomic) IBOutlet UILabel *messageTimeLabel;

@end
