//
//  MyPageMainViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 21/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "MyPageMainViewController.h"
#import "MyPageCollectionViewCell.h"
#import "AllChipsTableViewCell.h"
#import "AuthenticationViewController.h"
#import "FollowersViewController.h"
@interface MyPageMainViewController ()

@end

@implementation MyPageMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.dynamicView setHidden:NO];
    [self.collectionView setHidden:YES];
    [self.infoView setHidden:YES];
    [self.allTheChipsView setHidden:YES];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.applyForAllBtn.layer setCornerRadius:12];
    [self.responseBtn.layer setCornerRadius:10];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - collection Delegate


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 9;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
        MyPageCollectionViewCell *cell=(MyPageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"myPageCollectionViewCell" forIndexPath:indexPath];
       
        return cell;
   }

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    float cellWitdth = (collectionView.frame.size.width / 2) - 5;
    //float cellHeight = (255.0 / 218.0) * cellWitdth;
    return CGSizeMake(cellWitdth, cellWitdth);
   
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - tableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"AllChipsCell";
    AllChipsTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[AllChipsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"myPageToAllActivities" sender:self];
}


- (IBAction)menueActions:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            [self.dynamicView setHidden:NO];
            [self.collectionView setHidden:YES];
            [self.infoView setHidden:YES];
            [self.allTheChipsView setHidden:YES];

            break;
        }
        case 1:
        {
            [self.dynamicView setHidden:YES];
            [self.collectionView setHidden:NO];
            [self.infoView setHidden:YES];
            [self.allTheChipsView setHidden:YES];

            break;
        }
        case 2:
        {
            [self.dynamicView setHidden:YES];
            [self.collectionView setHidden:YES];
            [self.infoView setHidden:YES];
            [self.allTheChipsView setHidden:NO];
            break;
        }
        case 3:
        {
            [self.dynamicView setHidden:YES];
            [self.collectionView setHidden:YES];
            [self.infoView setHidden:NO];
            [self.allTheChipsView setHidden:YES];
            break;
        }
         
        default:
            break;
    }
    
    
    // changing button color
    UIColor *myThemeColor = [UIColor colorWithRed:78/255.0 green:101/255.0 blue:115/255.0 alpha:1];
    for (UIButton *btn in self.menuBtns) {
        if (btn.tag == sender.tag) {
            
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else{
            
            [btn setTitleColor:myThemeColor forState:UIControlStateNormal];
        }
    }
}

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)commentAction:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"AccountSettings" bundle:nil];
    AuthenticationViewController *viewController = (AuthenticationViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AuthenticationViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)fansAction:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"UmerDesigns" bundle:nil];
    FollowersViewController *viewController = (FollowersViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FollowersViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)atentionAction:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"UmerDesigns" bundle:nil];
    FollowersViewController *viewController = (FollowersViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FollowersViewController"];
    [self presentViewController:viewController animated:YES completion:nil];

}




@end
