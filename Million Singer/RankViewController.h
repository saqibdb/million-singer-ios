//
//  RankViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 12/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankTableViewCell.h"
@interface RankViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)listSelectionActions:(UIButton *)sender;
- (IBAction)subListSelectionActions:(UIButton *)sender;
- (IBAction)globalAndAreaActions:(UIButton *)sender;
- (IBAction)dateAction:(UIButton *)sender;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *listSelectionButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *subListSelectionButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *globalAndAreaButtons;
@property (weak, nonatomic) IBOutlet UIButton *dateButton;
@property (weak, nonatomic) IBOutlet UITableView *RankTableView;

@end
