//
//  PaperPlaneAndRunAwayViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "PaperPlaneAndRunAwayViewController.h"
#import "VideoLiveViewController.h"
@interface PaperPlaneAndRunAwayViewController ()

@end

@implementation PaperPlaneAndRunAwayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self activePaperPlaneView];
    [self.focusBtn.layer setCornerRadius:8];
    // Do any additional setup after loading the view.
}

-(void)activePaperPlaneView{
    [self.smallPlaneImageView setHidden:NO];
    [self.bigPlaneImageView setHidden:NO];
    [self.runAwayImageView setHidden:YES];
    self.centerLable.text = @"被主播放飛機了！";
    [self.focusBtn setTitle:@"退出" forState:UIControlStateNormal];
}

-(void)activeRunAwayView{
    [self.smallPlaneImageView setHidden:YES];
    [self.bigPlaneImageView setHidden:YES];
    [self.runAwayImageView setHidden:NO];
    self.centerLable.text = @"你關注的主播已經跑掉了";
    [self.focusBtn setTitle:@"關注其他主播" forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)FocusAction:(UIButton *)sender {
    if (sender.tag == 0) {
        sender.tag = 1;
        [self activeRunAwayView];
    }
    else
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"videoLive" bundle:nil];
        VideoLiveViewController *viewController = (VideoLiveViewController *)[storyboard instantiateViewControllerWithIdentifier:@"VideoLiveViewController"];
        [self presentViewController:viewController animated:YES completion:nil];
        
            }
}
- (IBAction)closeAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
