//
//  MallViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)positiveAndNagitiveAssessmentActions:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *positiveAndNagitiveAssessmentBtns;
- (IBAction)backAction:(UIButton *)sender;

@end
