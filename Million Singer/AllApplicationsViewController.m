//
//  AllApplicationsViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 12/09/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "AllApplicationsViewController.h"

@interface AllApplicationsViewController ()

@end

@implementation AllApplicationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
