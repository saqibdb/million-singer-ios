//
//  NewStarsTableViewCell.m
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "NewStarsTableViewCell.h"

@implementation NewStarsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.supportBtn.layer setCornerRadius:12];
    [self.reciprocalBtn.layer setCornerRadius:12];
    self.reciprocalBtn.layer.borderWidth = 1;
    UIColor *color = ([UIColor colorWithRed:197/255.0 green:240/255.0 blue:204/255.0 alpha:1]);
    [self.reciprocalBtn.layer setBorderColor:[color CGColor]];
    [self.reciprocalBtn.layer setBorderWidth:1];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
