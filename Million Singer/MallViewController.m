//
//  MallViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "MallViewController.h"
#import "MallTableViewCell.h"
@interface MallViewController ()
{
    NSInteger senderTag;
}
@end

@implementation MallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    senderTag = 0;
    self.tableView.dataSource = self;
    self.tableView.delegate =self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (senderTag == 0) {
        return 3;
    }
    else
    {
        return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MallCell";
    MallTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[MallTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    
    if (senderTag == 0) {
        switch (indexPath.row) {
            case 0:
            {
                [cell.quantitiyBtn setTitle:@"1" forState:UIControlStateNormal];
                [cell.itemTypeImage setImage:[UIImage imageNamed:@"messageFlowerIcon"]];
                cell.nameLabel.text = @"花";
                break;
            }
            case 1:
            {
                [cell.quantitiyBtn setTitle:@"1" forState:UIControlStateNormal];
                [cell.itemTypeImage setImage:[UIImage imageNamed:@"giftIcon"]];
                cell.nameLabel.text = @"禮物";
                
                break;
            }
                
            case 2:
            {
                [cell.quantitiyBtn setTitle:@"1" forState:UIControlStateNormal];
                [cell.itemTypeImage setImage:[UIImage imageNamed:@"voleumIcon"]];
                cell.nameLabel.text = @"參賽卷";
                
                break;
            }
                
            default:
                break;
        }
    }
    else
    {
        switch (indexPath.row) {
            case 0:
            {
                [cell.quantitiyBtn setTitle:@"1" forState:UIControlStateNormal];
                [cell.itemTypeImage setImage:[UIImage imageNamed:@"bombIcon"]];
                cell.nameLabel.text = @"炸彈";
                break;
            }
            case 1:
            {
                [cell.quantitiyBtn setTitle:@"1" forState:UIControlStateNormal];
                [cell.itemTypeImage setImage:[UIImage imageNamed:@"canonIcon"]];
                cell.nameLabel.text = @"大炮";
                
                break;
            }
            default:
                break;
        }
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPat{
    
}

#pragma mark - Actions

- (IBAction)positiveAndNagitiveAssessmentActions:(UIButton *)sender {
    UIColor *myThemeColor = [UIColor colorWithRed:78/255.0 green:101/255.0 blue:115/255.0 alpha:1];
    for (UIButton *btn in self.positiveAndNagitiveAssessmentBtns) {
        if (btn.tag == sender.tag) {
            
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else{
            
            [btn setTitleColor:myThemeColor forState:UIControlStateNormal];
        }
    }
    
    
    if (sender.tag == 0) {
        senderTag = 0;
    }
    else
    {
        senderTag = 1;
    }
    [self.tableView reloadData];

}
- (IBAction)backAction:(UIButton *)sender {
   [self dismissViewControllerAnimated:YES completion:nil];
    }
@end
