//
//  PhoneVerificationViewController.h
//  Million Singer
//
//  Created by Ali Apple  on 8/2/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneVerificationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *countryText;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeText;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextF;


@property (weak, nonatomic) IBOutlet UIButton *verificationCodeBtn;

@property (weak, nonatomic) IBOutlet UILabel *verificationDescriptionText;
- (IBAction)countryAction:(UIButton *)sender;
- (IBAction)verificationCodeAction:(UIButton *)sender;


@end
