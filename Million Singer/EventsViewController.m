//
//  EventsViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 16/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "EventsViewController.h"
#import "EventsTableViewCell.h"
#import "SingersCountryCollectionViewCell.h"
#import "Localisator.h"
@interface EventsViewController ()

@end

@implementation EventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate= self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self showEventsList];
    
    [self.activityBtn setTitle:LOCALIZATION(@"activity") forState:UIControlStateNormal];
    [self.filmBtn setTitle:LOCALIZATION(@"film") forState:UIControlStateNormal];
   
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
[self handleCalenderView ];
    
}

-(void)handleCalenderView{
    
    UIColor *weekDayColor = [UIColor colorWithRed:78/255.0 green:167/255.0 blue:213/255.0 alpha:1];
    UIColor *weekEndDayColor = [UIColor colorWithRed:120/255.0 green:213/255.0 blue:116/255.0 alpha:1];
    UIColor *componentTextColor = [UIColor colorWithRed:227/255.0 green:245/255.0 blue:255/255.0 alpha:1];
    self.calenderView.weekdayHeaderTextColor = weekDayColor;
    self.calenderView.weekdayHeaderWeekendTextColor = weekEndDayColor;
    self.calenderView.componentTextColor = componentTextColor;
    self.calenderView.todayIndicatorColor = weekEndDayColor;
    self.calenderView.indicatorRadius = 15;
     [self.calenderView reloadViewAnimated:YES];
    
}

-(void)showEventsList{
    [self.tableView setHidden:NO];
    [self.collectionView setHidden:YES];
    self.rankBtnTrailingconstraint.constant = -45;
}

-(void)showVideosCollection{
    [self.tableView setHidden:YES];
    [self.collectionView setHidden:NO];
    self.rankBtnTrailingconstraint.constant = 5;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - tableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"eventCell";
    EventsTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[EventsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:MyIdentifier];
    }
    
    cell.lblSingTheCup.text = LOCALIZATION(@"Sing the championship cup");
    cell.lblCountry.text = LOCALIZATION(@"hong_kong");
    cell.lblBriefIntro.text = LOCALIZATION(@"brief introduction");
    cell.lblOver.text = LOCALIZATION(@"over");
    cell.lblTime.text = LOCALIZATION(@"minute");
    //cell.lblTag.text = LOCALIZATION(@"");
    [cell.watchLiveBtn setTitle:LOCALIZATION(@"Watch live") forState:UIControlStateNormal];
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"eventsToEventsDetail" sender:self];
    
}

#pragma mark - collectionView Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 10.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 9;}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SingersCountryCollectionViewCell *cell=(SingersCountryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SingersCountryCollectionViewCell" forIndexPath:indexPath];
    [cell layoutIfNeeded];
    cell.profilePictureBorderView.cornerRadius = cell.profilePictureBorderView.frame.size.width / 2;
    cell.countryBorderView.cornerRadius = cell.countryBorderView.frame.size.width / 2;
   // cell.songNameBorderView.cornerRadius = cell.songNameBorderView.frame.size.height / 2;
    cell.backgroundColor = [UIColor clearColor];
    [cell layoutIfNeeded];
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float cellWitdth = (collectionView.frame.size.width / 2) - 5;
//    float cellHeight = (255.0 / 218.0) * cellWitdth;
    float cellHeight = 255.0;

    return CGSizeMake(cellWitdth, cellHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - Actions
- (IBAction)eventsAction:(UIButton *)sender {
    [self showEventsList];
}

- (IBAction)eventVideosAction:(UIButton *)sender {
    [self showVideosCollection];
}

- (IBAction)searchAction:(UIButton *)sender {
    self.calenderView.singleRowMode = YES;
}

- (IBAction)rankAction:(UIButton *)sender {
    self.calenderView.singleRowMode = NO;

}
@end
