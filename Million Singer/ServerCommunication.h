//
//  ServerCommunication.h
//  Million Singer
//
//  Created by iBuildx-Macbook on 13/10/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

typedef void (^APIRequestResponseBlock) (id object, BOOL status, NSError *error);//block as a typedef

@interface ServerCommunication : NSObject

+(void)loginWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)VerifyOtpCodeWithCode :(NSString *)code  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)updateUsersProfile :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)userProfileView:(APIRequestResponseBlock)responseBlock;
+(void)logout:(APIRequestResponseBlock)responseBlock;
+(void)userImage:(UIImage *)image andResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void) GetJsonResponsea :(APIRequestResponseBlock)responseBlock;
@end
