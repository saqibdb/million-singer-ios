//
//  EventsTableViewCell.h
//  Million Singer
//
//  Created by MACBOOK PRO on 16/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIButton *watchLiveBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblSingTheCup;
@property (weak, nonatomic) IBOutlet UILabel *lblBriefIntro;
@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblOver;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTag;

@end
