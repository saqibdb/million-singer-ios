//
//  CenterMenuViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 31/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "CenterMenuViewController.h"
#import "MyFilmsViewController.h"
#import "FilmViewController.h"
#import "Localisator.h"
@interface CenterMenuViewController ()

@end

@implementation CenterMenuViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [self setLayouts];
    self.lblVideo.text = LOCALIZATION(@"Video");
    self.lblLive.text = LOCALIZATION(@"Live");
    self.lblFilm.text = LOCALIZATION(@"film");
    self.lblDraft.text = LOCALIZATION(@"draft");
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{

    [self.tabBarController.tabBar setHidden:YES];
    [[self.tabBarController.view.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, 320, 480)];
}
-(void)setLayouts{
    [self.view layoutIfNeeded];
    for (UIView *view in self.mainViews) {
        [view.layer setCornerRadius:view.frame.size.height/2];
        view.clipsToBounds = YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)recordingAction:(UIButton *)sender {
}

- (IBAction)playAction:(UIButton *)sender {
}

- (IBAction)explorerAction:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MenueVideo" bundle:nil];
    FilmViewController *viewController = (FilmViewController *)[storyboard instantiateViewControllerWithIdentifier:@"FilmViewController"];
    
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:viewController];
    [navController.navigationBar setHidden:YES];
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)folderAction:(UIButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MenueVideo" bundle:nil];
    MyFilmsViewController *viewController = (MyFilmsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MyFilmsViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)mainBtnAction:(UIButton *)sender {
    [self.tabBarController.tabBar setHidden:NO];
    self.tabBarController.selectedIndex = 0;
}
@end
