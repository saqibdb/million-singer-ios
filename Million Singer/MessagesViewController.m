//
//  MessagesViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessagesTableViewCell.h"
#import "Localisator.h"
@interface MessagesViewController ()

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self setLocalizedTitles];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLocalizedTitles{

    
    self.titleLabel.text = LOCALIZATION(@"information");
    [self.btnAutoReply setTitle:LOCALIZATION(@"Automatic reply") forState:UIControlStateNormal];
    
    
    for (UIButton *btn in self.topBarBtns) {
        if (btn.tag == 0) {
            [btn setTitle:LOCALIZATION(@"private information") forState:UIControlStateNormal];
        }
        else if(btn.tag == 1){
            [btn setTitle:LOCALIZATION(@"Notice") forState:UIControlStateNormal];
        }
        else if(btn.tag == 2){
            [btn setTitle:LOCALIZATION(@"system message") forState:UIControlStateNormal];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"messageCell";
    MessagesTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[MessagesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
    }
    [cell.replyBtn setTitle:LOCALIZATION(@"Reply") forState:UIControlStateNormal];
      return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPat{
    
}

#pragma mark - Actions

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)autoReplyAction:(UIButton *)sender {
}

- (IBAction)privateInfoAction:(UIButton *)sender {
}

- (IBAction)noticeAction:(UIButton *)sender {
}

- (IBAction)systemMessage:(UIButton *)sender {
}
- (IBAction)topBarActions:(UIButton *)sender {
    
    UIColor *myThemeColor = [UIColor colorWithRed:78/255.0 green:101/255.0 blue:115/255.0 alpha:1];
    for (UIButton *btn in self.topBarBtns) {
        if (btn.tag == sender.tag) {
            
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
        else{
            
            [btn setTitleColor:myThemeColor forState:UIControlStateNormal];
        }
    }

}
@end
