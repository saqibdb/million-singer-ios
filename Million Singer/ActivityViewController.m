//
//  ActivityViewController.m
//  Million Singer
//
//  Created by ibuildx on 8/11/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ActivityViewController.h"
#import "UIColor+Additions.h"

@interface ActivityViewController ()

@end

@implementation ActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self.view layoutIfNeeded];
    
    [self.wheelView addTarget:self action:@selector(wheelDidChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.wheelView insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WheelBackground"]] atIndex:0];
    self.wheelView.delegate = self;
    self.wheelView.dataSource = self;
    
    
    //self.wheelView.frame = CGRectMake(0, 0, self.wheelView.frame.size.width, self.wheelView.frame.size.height);
    
    [self.wheelView reloadData];

    [self.view layoutIfNeeded];
    
    self.wheelButtonVeiw.cornerRadius = self.wheelButtonVeiw.frame.size.width / 2;
    
    self.wheelButtonVeiw.layer.masksToBounds = NO;
    self.wheelButtonVeiw.layer.shadowOffset = CGSizeMake(0, 0);
    self.wheelButtonVeiw.layer.shadowRadius = 5;
    self.wheelButtonVeiw.layer.shadowOpacity = 0.5;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - Wheel delegate

- (void)wheelDidEndDecelerating:(SMWheelControl *)wheel
{
    
}

- (void)wheel:(SMWheelControl *)wheel didRotateByAngle:(CGFloat)angle
{
    
}

#pragma mark - Wheel dataSource

- (NSUInteger)numberOfSlicesInWheel:(SMWheelControl *)wheel
{
    return 8;
}

- (UIView *)wheel:(SMWheelControl *)wheel viewForSliceAtIndex:(NSUInteger)index
{
    
    
    NSLog(@"Wheel View Width = %f" , wheel.frame.size.width);
    
    
    
    CGFloat cellWidth = (wheel.frame.size.width / 2) - 10;
    CGFloat cellHeight = (2 * M_PI * cellWidth) / 8;
    cellHeight = cellHeight + 8;
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint:CGPointMake(0, 0)];
    
    //[trianglePath addLineToPoint:CGPointMake(0,cellHeight)];
    [trianglePath addArcWithCenter:CGPointMake(cellWidth, cellHeight/2) radius:cellWidth startAngle:0 endAngle:0.7 clockwise:YES];
    
    [trianglePath addLineToPoint:CGPointMake(cellWidth, cellHeight/2)];
    [trianglePath closePath];
    
    CAShapeLayer *triangleMaskLayer = [CAShapeLayer layer];
    [triangleMaskLayer setPath:trianglePath.CGPath];
    
    
    
    UIView *traingleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(50, (cellHeight / 2) - 15, 150, 30)];
    

    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"PingFang SC-Semibold" size:20];
    
    
    if (index%2) {
        traingleView.backgroundColor =  [UIColor add_colorWithRGBHexString:@"0E5D81"];
        label.text = @"10";
    }
    else{
        traingleView.backgroundColor = [UIColor add_colorWithRGBHexString:@"00B2FB"];
        label.text = @"100";
    }
    if (index == 3) {
        label.text = @"1,000";
        traingleView.backgroundColor =  [UIColor add_colorWithRGBHexString:@"168D89"];
    }
    
    
    //traingleView.layer.mask = triangleMaskLayer;

    
    
    
    UIBezierPath* trianglePathSimple = [UIBezierPath bezierPath];
    [trianglePathSimple moveToPoint:CGPointMake(0, 0)];
    [trianglePathSimple addLineToPoint:CGPointMake(0, cellHeight)];
    [trianglePathSimple addLineToPoint:CGPointMake(cellWidth, cellHeight/2 )];
    [trianglePathSimple closePath];

    CAShapeLayer *triangleMaskLayerSimple = [CAShapeLayer layer];
    [triangleMaskLayerSimple setPath:trianglePathSimple.CGPath];
    triangleMaskLayerSimple.borderWidth = 2;
    triangleMaskLayerSimple.borderColor = [[UIColor whiteColor] CGColor];
    
    traingleView.layer.mask = triangleMaskLayerSimple;
    
    
    
    //traingleView.layer.borderWidth = 2;
    //traingleView.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    
    UIImageView *dollarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (cellHeight/2) - 15, 30, 30)];
    
    dollarImageView.image = [UIImage imageNamed:@"coinIcon"];
    [traingleView addSubview:dollarImageView];

    [traingleView addSubview:label];
    return traingleView;
}

#pragma mark - Wheel Control

- (void)wheelDidChangeValue:(id)sender
{
    //self.valueLabel.text = [NSString stringWithFormat:@"Selected index: %d", self.wheel.selectedIndex];
    NSLog(@"%@",[NSString stringWithFormat:@"Selected index: %lu", (unsigned long)self.wheelView.selectedIndex]);
}

- (CGFloat)snappingAngleForWheel:(id)sender
{
    return M_PI / 2;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)drawNowAction:(UIButton *)sender {
    NSInteger randomNumber = arc4random() % 7;

    [self.wheelView setSelectedIndex:randomNumber animated:YES];
    NSLog (@"Font families: %@", [UIFont familyNames]);

}
@end
