//
//  VideoLiveViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 29/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoLiveViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *commentsView;
@property (weak, nonatomic) IBOutlet UITextField *txtComment;
@property (weak, nonatomic) IBOutlet UIView *giftsView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionVew;
- (IBAction)bottomMenuActions:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *sharePopUpView;
@property (weak, nonatomic) IBOutlet UIView *userInfoView;
- (IBAction)determineAction:(UIButton *)sender;
- (IBAction)attentionAction:(UIButton *)sender;
- (IBAction)showUserInfoAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *profitView;
- (IBAction)redPopUpDetermineAction:(UIButton *)sender;
- (IBAction)showProfitViewAction:(UIButton *)sender;

@end
