//
//  InterestCountryCollectionViewCell.h
//  Million Singer
//
//  Created by Ali Apple  on 8/7/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>


@interface InterestCountryCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet CCMBorderView *profileBorderView;
@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;

@property (weak, nonatomic) IBOutlet UILabel *countryName;

@property (weak, nonatomic) IBOutlet UIView *maskView;

@property (weak, nonatomic) IBOutlet UIImageView *numberIcon;

@property (weak, nonatomic) IBOutlet UILabel *selectedCountryNumber;




@end
