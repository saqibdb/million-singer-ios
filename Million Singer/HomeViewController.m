//
//  HomeViewController.m
//  Million Singer
//
//  Created by ibuildx on 8/3/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "HomeViewController.h"
#import "GoldCoinsPopUpView.h"
#import "BindCellPopUpView.h"
#import "UIViewController+CWPopup.h"
#import "InterestCountryCollectionViewCell.h"
#import "UIColor+Additions.h"
#import "ActivitiesCollectionViewCell.h"
#import "SingersCountryCollectionViewCell.h"
#import <ChameleonFramework/Chameleon.h>
#import "VideoLiveViewController.h"
#import "Localisator.h"
@interface HomeViewController (){
    NSArray *videoTypes;
    NSArray *countryFlagsArray;
    NSArray *countryNamesArray;
    NSMutableArray *selectedCountry;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedCountry = [[NSMutableArray alloc]init];
      self.bottomText.text = [NSString stringWithFormat:@"已選取：1.香港"];
    // Do any additional setup after loading the view.
    [self setupSlider];
    [self setupTypePicker];
    self.useBlurForPopup = YES;
    [self setupHongKongTimeAndDate];
    self.countryCollectionView.delegate = self;
    self.countryCollectionView.dataSource = self;
    self.countryCollectionView.allowsMultipleSelection = YES;
    self.activitiesCollection.delegate = self;
    self.activitiesCollection.dataSource = self;
    
    self.singerCountryCollectionView.delegate = self;
    self.singerCountryCollectionView.dataSource = self;

    self.countryCollectionView.backgroundColor = [UIColor clearColor];
    self.activitiesCollection.backgroundColor = [UIColor clearColor];
    self.singerCountryCollectionView.backgroundColor = [UIColor clearColor];

    self.coinsBtn.badgeString = @"2";
    self.letterBtn.badgeString = @"2";
    self.coinsBtn.badgeBackgroundColor = [UIColor add_colorWithRGBHexString:@"F41313"];
    self.letterBtn.badgeBackgroundColor = [UIColor add_colorWithRGBHexString:@"F41313"];
    [self.coinsBtn setBadgeEdgeInsets:UIEdgeInsetsMake(17, -10, 0, 0)];
    [self.letterBtn setBadgeEdgeInsets:UIEdgeInsetsMake(17, -10, 0, 0)];
    self.bottomSpaceConstraint1.constant = self.view.frame.size.height;
    [self.view layoutIfNeeded];
    
    
    [self.view layoutIfNeeded];
    UIColor *colorHigh = [UIColor add_colorWithRGBHexString:@"861818"];
    UIColor *lowColor = [UIColor add_colorWithRGBHexString:@"470F64"];
    
    
    self.headerGradientView.backgroundColor = [[UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:self.headerGradientView.frame andColors:[NSArray arrayWithObjects: lowColor, colorHigh, nil]] colorWithAlphaComponent:0.5];
    

    [self setCountryFlags];
    [self.moreActivitiesBtn setTitle:LOCALIZATION(@"more_activities") forState:UIControlStateNormal];
    [self.moreBtn setTitle:LOCALIZATION(@"more") forState:UIControlStateNormal];
    self.lblCountryName.text = LOCALIZATION(@"hong_kong");
    [self.view layoutIfNeeded];
    
    
}
-(void)setCountryFlags{
    countryFlagsArray = [[NSArray alloc]initWithObjects:@"flag0",@"flag1",@"flag2",@"flag3",@"flag4",@"flag5",@"flag6",@"flag7",@"flag8",@"flag9",@"flag10",@"flag11",@"flag12",@"flag13",@"flag14",@"flag14", nil];
    
    countryNamesArray = [[NSArray alloc]initWithObjects:@"巴西",@"德國",@"中國",@"香港",@"日本",@"美國",@"英國",@"愛爾蘭",@"意大利",@"俄羅斯",@"法國",@"韓國",@"土耳其",@"瑞士",@"加拿大",@"英格蘭", nil];
    [self.countryCollectionView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    //[self openCoinsPopup];
}

-(void)setupSlider {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    [data addObject:@"https://i.ytimg.com/vi/8pPvNqOb6RA/hqdefault.jpg"];
    [data addObject:@"https://3.bp.blogspot.com/-TQzpzSYvixs/V5fhuKsWn8I/AAAAAAAAAIw/diy_QsEtHMkPJPx8SF_Fr_fqYx0DldvrACLcB/s1600/U2-With-Or-Without-Y-351964.jpg"];
    self.mainSlider.slides = data;
    [self.mainSlider setupSliderView];
}

-(void)setupHongKongTimeAndDate{

    [NSTimeZone abbreviationDictionary];
    NSInteger no_of_seconds = [[NSTimeZone timeZoneWithAbbreviation:@"HKT"] secondsFromGMT];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"香港 h:mm a"];
    
    
    NSString *strToday = [timeFormatter  stringFromDate:[NSDate dateWithTimeIntervalSinceNow:no_of_seconds]];
    NSLog(@"%@",strToday);
    //self.mainSlider.hongKongTimeLabel.text = strToday;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE, MMMM dd, yyyy"];
    
    NSString *strDate = [dateFormatter  stringFromDate:[NSDate dateWithTimeIntervalSinceNow:no_of_seconds]];
    NSLog(@"%@",strDate);
    //self.mainSlider.hongKongDateLabel.text = strDate;
    
}


-(void)setupTypePicker {
    videoTypes = [NSArray arrayWithObjects:LOCALIZATION(@"Live"), LOCALIZATION(@"film"), nil];
    self.typePickerView.delegate = self;
    self.typePickerView.dataSource = self;
    self.typePickerView.interitemSpacing = self.view.frame.size.width / 20;
    self.typePickerView.textColor = [UIColor add_colorWithRGBHexString:@"688591"];
    self.typePickerView.highlightedTextColor = [UIColor add_colorWithRGBHexString:@"B5D4E4"];
    [self.typePickerView setFont:[UIFont fontWithName:@"PingFang TC" size:14]];
    [self.typePickerView setHighlightedFont:[UIFont fontWithName:@"PingFang TC" size:17]];
}

- (void)openCoinsPopup {
    GoldCoinsPopUpView *goldCoinsPopUpView = [[GoldCoinsPopUpView alloc] initWithNibName:@"GoldCoinsPopUpView" bundle:nil];
    goldCoinsPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y/2, self.view.frame.size.width- 100, (self.view.frame.size.width- 100) * 0.68 );
    [goldCoinsPopUpView.view layoutIfNeeded];
    goldCoinsPopUpView.topSpaceConstraint.constant = (goldCoinsPopUpView.topBorderView.frame.size.width / 2) * -1;
    goldCoinsPopUpView.topBorderView.cornerRadius = goldCoinsPopUpView.topBorderView.frame.size.width / 2;
    [goldCoinsPopUpView.view layoutIfNeeded];
    [self presentPopupViewController:goldCoinsPopUpView animated:YES completion:^(void) {
        [goldCoinsPopUpView.shareBtn addTarget:self action:@selector(dismissPopupWithNextPopupOpen) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)cellBindPopup {
    BindCellPopUpView *bindCellPopUpView = [[BindCellPopUpView alloc] initWithNibName:@"BindCellPopUpView" bundle:nil];
    bindCellPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y/2, self.view.frame.size.width- 100, (self.view.frame.size.width- 100) * 0.68 );
    [bindCellPopUpView.view layoutIfNeeded];
    bindCellPopUpView.topSpaceConstraint.constant = (bindCellPopUpView.topBorderView.frame.size.width / 2) * -1;
    bindCellPopUpView.topBorderView.cornerRadius = bindCellPopUpView.topBorderView.frame.size.width / 2;
    [bindCellPopUpView.view layoutIfNeeded];
    [self presentPopupViewController:bindCellPopUpView animated:YES completion:^(void) {
        [bindCellPopUpView.shareBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)dismissPopupWithNextPopupOpen {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
        }];
    }
}

- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
            //[self performSegueWithIdentifier:@"ToCountries" sender:self];
        }];
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 10.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([collectionView isEqual:self.activitiesCollection]) {
        return 8;
    }
     if ([collectionView isEqual:self.singerCountryCollectionView]){
        return 10;
    }
    if ([collectionView isEqual:self.countryCollectionView]) {
        return countryNamesArray.count;
    }
    return 0;
    
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView isEqual:self.activitiesCollection]) {
        //
        ActivitiesCollectionViewCell *cell=(ActivitiesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ActivitiesCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
     if ([collectionView isEqual:self.singerCountryCollectionView]){
        
        SingersCountryCollectionViewCell *cell=(SingersCountryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SingersCountryCollectionViewCell" forIndexPath:indexPath];
        [cell layoutIfNeeded];
        cell.profilePictureBorderView.cornerRadius = cell.profilePictureBorderView.frame.size.width / 2;
        cell.countryBorderView.cornerRadius = cell.countryBorderView.frame.size.width / 2;
        [cell.goToUserPageBtn addTarget:self action:@selector(gotoUserProfile:) forControlEvents:UIControlEventTouchUpInside];
    
        cell.songNameBorderView.cornerRadius = cell.songNameBorderView.frame.size.height / 2;
        
        cell.backgroundColor = [UIColor clearColor];
        [cell layoutIfNeeded];
        return cell;
        
        
    }
     if ([collectionView isEqual:self.countryCollectionView]) {
        InterestCountryCollectionViewCell *cell=(InterestCountryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"InterestCountryCollectionViewCell" forIndexPath:indexPath];
        [cell layoutIfNeeded];
        cell.profileBorderView.cornerRadius = cell.profileBorderView.frame.size.width / 2;
        cell.maskView.hidden = YES;
        
        if (indexPath.row == 3) {
            cell.maskView.hidden = NO;
            cell.selectedCountryNumber.text = @"1";
            [cell setUserInteractionEnabled:NO];
        }
         else
         {
             cell.maskView.hidden = YES;
             cell.selectedCountryNumber.text = @"";
             [cell setUserInteractionEnabled:YES];
             
         }
        
        NSString *imageName = [countryFlagsArray objectAtIndex:indexPath.row];
        NSString *countryName = [countryNamesArray objectAtIndex:indexPath.row];
        
        [cell.countryImageView setImage:[UIImage imageNamed:imageName]];
        cell.countryName.text = countryName;
        
        
        
        
        cell.backgroundColor = [UIColor clearColor];
        [cell layoutIfNeeded];
        return cell;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([collectionView isEqual:self.activitiesCollection]) {
        return CGSizeMake(105.0, 105.0);
    }
    else if ([collectionView isEqual:self.singerCountryCollectionView]){
        float cellWitdth = (collectionView.frame.size.width / 2) - 5;
//        float cellHeight = (255.0 / 218.0) * cellWitdth;
        float cellHeight = 255.0;
        return CGSizeMake(cellWitdth, cellHeight);
    }
    else{
        float cellWitdth = (collectionView.frame.size.width / 4) - 10;
        float cellHeight = (127.0 / 71.0) * cellWitdth;
        return CGSizeMake(cellWitdth, cellHeight);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([collectionView isEqual:self.activitiesCollection]) {
        if (indexPath.row == 0) {
            [self performSegueWithIdentifier:@"homeToNewStars" sender:self];

        }
        else
        {
            [self performSegueWithIdentifier:@"homeToCompetitionEventsVC" sender:self];

        }
    }
    if ([collectionView isEqual:self.singerCountryCollectionView]) {
        //[self performSegueWithIdentifier:@"homeToPaperPlaneView" sender:self];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"videoLive" bundle:nil];
        VideoLiveViewController *viewController = (VideoLiveViewController *)[storyboard instantiateViewControllerWithIdentifier:@"VideoLiveViewController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
    
    if ([collectionView isEqual:self.countryCollectionView]) {
        if (selectedCountry.count < 2) {
            InterestCountryCollectionViewCell *cell =  (InterestCountryCollectionViewCell *)[self.countryCollectionView cellForItemAtIndexPath:indexPath];
            [cell.maskView setHidden:NO];
            [selectedCountry addObject:indexPath];
            cell.selectedCountryNumber.text = [NSString stringWithFormat:@"%lu",selectedCountry.count+1];
        }
        else
        {
        
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Sorry"
                                         message:@"Maximum number of countries selected."
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* OkBtn = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        
                                        //Handle your yes please button action here
                                    }];
            [alert addAction:OkBtn];
            [self presentViewController:alert animated:YES completion:nil];
        }
        [self resetListInBottomText];
 
     }
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ([collectionView isEqual:self.countryCollectionView]) {
        InterestCountryCollectionViewCell *cell =  (InterestCountryCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [cell.maskView setHidden:YES];
        [selectedCountry removeObject:indexPath];
        for (int i = 0; i<selectedCountry.count; i++) {
            NSIndexPath *ip = [selectedCountry objectAtIndex:i];
            InterestCountryCollectionViewCell *cellsToReset =  (InterestCountryCollectionViewCell *)[collectionView cellForItemAtIndexPath:ip];
            cellsToReset.selectedCountryNumber.text = [NSString stringWithFormat:@"%d",i+2];
        }
        [self resetListInBottomText];
    }
}

-(void)resetListInBottomText{

    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<selectedCountry.count; i++) {
        NSIndexPath *ip = [selectedCountry objectAtIndex:i];
        NSString *string = [countryNamesArray objectAtIndex:ip.row];
        [tempArray addObject:string];
    }
    
    
    if (tempArray.count == 1) {
        self.bottomText.text = [NSString stringWithFormat:@"已選取：1.香港  2.%@",tempArray[0]];
    }
    else if (tempArray.count == 2)
    {
        self.bottomText.text = [NSString stringWithFormat:@"已選取：1.香港  2.%@  3.%@",tempArray[0],tempArray[1]];
    }
    else
    {
         self.bottomText.text = [NSString stringWithFormat:@"已選取：1.香港"];
    }
   

    
}

- (IBAction)gotoUserProfile:(UIButton *)sender {
    [self performSegueWithIdentifier:@"homeToUserPage" sender:self];
}

-(void)openCountriesView {
    [self.view layoutIfNeeded];
    self.bottomSpaceConstraint1.constant = 0;
    [UIView animateWithDuration:1
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
}

-(void)closeCountriesView {
    [self.view layoutIfNeeded];
    self.bottomSpaceConstraint1.constant = self.view.frame.size.height;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
}





- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView{
    return videoTypes.count;
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item{
    return videoTypes[item];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item{
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)tickAction:(UIButton *)sender {
    [self closeCountriesView];
}

- (IBAction)countryAction:(UIButton *)sender {
    [self openCountriesView];
}

- (IBAction)searchAction:(UIButton *)sender {
    //[self openCoinsPopup];
    [self performSegueWithIdentifier:@"homeToSearchViewController" sender:self];
}

- (IBAction)rankAction:(UIButton *)sender {
   // [self cellBindPopup];
    [self performSegueWithIdentifier:@"homeToRankingVC" sender:self];
}

- (IBAction)coinsAction:(MIBadgeButton *)sender {
    
    [self performSegueWithIdentifier:@"homeToRechargeVC" sender:self];

}

- (IBAction)letterAction:(MIBadgeButton *)sender {
    [self performSegueWithIdentifier:@"homeToMessages" sender:self];
    
}
- (IBAction)unwindToHome:(UIStoryboardSegue *)sender {
    
    
}


- (IBAction)moreActivitiesAction:(UIButton *)sender {
}
- (IBAction)moreAction:(UIButton *)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"under construction"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* OkBtn = [UIAlertAction
                            actionWithTitle:@"Ok"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action) {
                                
                                //Handle your yes please button action here
                            }];
    [alert addAction:OkBtn];
    [self presentViewController:alert animated:YES completion:nil];
    
}
@end
