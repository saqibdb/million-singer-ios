//
//  UserModel.h
//  Million Singer
//
//  Created by MACBOOK PRO on 13/10/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface UserModel : JSONModel

@property (nonatomic) NSString * updated_date;
@property (nonatomic) NSString *intro;
@property (nonatomic) NSNumber *age;
@property (nonatomic) NSString *dob;
@property (nonatomic) NSString *nick_name;
@property (nonatomic) NSNumber * user_id;
@property (nonatomic) NSNumber * registration_type;
@property (nonatomic) NSString *created_date;
@property (nonatomic) NSString *location;
@property (nonatomic) NSString *image;
@property (nonatomic) NSString *number;
@property (nonatomic) NSNumber *verify_no;
@property (nonatomic) NSNumber *lang;
@property (nonatomic) NSString *device_id;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *gender;
@property (nonatomic) NSNumber * status;
@end
