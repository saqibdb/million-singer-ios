//
//  ExploreViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 28/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ExploreViewController.h"
#import "SingersCountryCollectionViewCell.h"
#import "ExploreTableViewCell.h"
#import "Localisator.h"
@interface ExploreViewController ()

@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.charachterTableView.dataSource = self;
    self.charachterTableView.delegate =self;
    [self.charachterTableView setHidden:YES];
    [self.collectionView setHidden:NO];
    self.attentionTitle.text = LOCALIZATION(@"attention");
    for (UIButton *Btn in self.menuBtns) {
        if (Btn.tag == 0) {
            [Btn setTitle:LOCALIZATION(@"attention") forState:UIControlStateNormal];
        }
        else if (Btn.tag == 1){
        [Btn setTitle:LOCALIZATION(@"character") forState:UIControlStateNormal];
            
        }
    }
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - collection View Delegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 12;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
        SingersCountryCollectionViewCell *cell=(SingersCountryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SingersCountryCollectionViewCell" forIndexPath:indexPath];
        [cell layoutIfNeeded];
        cell.profilePictureBorderView.cornerRadius = cell.profilePictureBorderView.frame.size.width / 2;
        cell.countryBorderView.cornerRadius = cell.countryBorderView.frame.size.width / 2;
       // [cell.goToUserPageBtn addTarget:self action:@selector(gotoUserProfile:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.songNameBorderView.cornerRadius = cell.songNameBorderView.frame.size.height / 2;
        
        cell.backgroundColor = [UIColor clearColor];
        [cell layoutIfNeeded];
        return cell;
        
        
    }

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
           float cellWitdth = (collectionView.frame.size.width / 2) - 5;
//        float cellHeight = (255.0 / 218.0) * cellWitdth;
        float cellHeight = 255.0;

        return CGSizeMake(cellWitdth, cellHeight);
   
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"exploreToLiveVideo" sender:self];
   
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 6;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"exploreCell";
    ExploreTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[ExploreTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPat{
    
}

- (IBAction)countryAction:(UIButton *)sender {
}

- (IBAction)searchAction:(UIButton *)sender {
}

- (IBAction)rankAction:(UIButton *)sender {
}
- (IBAction)menuActions:(UIButton *)sender {
    
    
    
    UIColor *myThemeColor = [UIColor colorWithRed:78/255.0 green:101/255.0 blue:115/255.0 alpha:1];
    for (UIButton *btn in self.menuBtns) {
        if (btn.tag == sender.tag) {
            
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else{
            
            [btn setTitleColor:myThemeColor forState:UIControlStateNormal];
        }
    }

    
    if (sender.tag == 0) {
        [self.charachterTableView setHidden:YES];
        [self.collectionView setHidden:NO];
    }
    else
    {
        [self.charachterTableView setHidden:NO];
        [self.collectionView setHidden:YES];
    }
}
@end
