//
//  ExploreViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 28/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)countryAction:(UIButton *)sender;
- (IBAction)searchAction:(UIButton *)sender;
- (IBAction)rankAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *charachterTableView;
- (IBAction)menuActions:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *menuBtns;
@property (weak, nonatomic) IBOutlet UILabel *attentionTitle;

@end
