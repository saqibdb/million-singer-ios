//
//  UserData.h
//  AFNetworkingCustomObjectiveC
//
//  Created by ibuildx-Mac on 8/21/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>

@interface UserData : JSONModel

@property (nonatomic) NSString *userID;
@property (nonatomic) NSString *userName;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;

@end
