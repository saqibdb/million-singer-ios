//
//  PaperPlaneAndRunAwayViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaperPlaneAndRunAwayViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *runAwayImageView;

@property (weak, nonatomic) IBOutlet UIImageView *smallPlaneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bigPlaneImageView;
@property (weak, nonatomic) IBOutlet UILabel *centerLable;
- (IBAction)FocusAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *focusBtn;
- (IBAction)closeAction:(UIButton *)sender;



@end
