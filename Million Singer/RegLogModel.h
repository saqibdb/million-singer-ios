//
//  RegLogModel.h
//  Million Singer
//
//  Created by iBuildx-Macbook on 13/10/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface RegLogModel : JSONModel

@property (nonatomic) NSInteger userid;
@property (nonatomic) NSString *session_key;
@property (nonatomic) NSNumber<Optional> *verify_no;
@property (nonatomic) BOOL error;




@end
/*
 {"error":false,"userid":21,"session_key":"EwM0RqqPeLTjam2OyTCy","verify_no":2,"statuscode":200,"msg":"Success"}
 */
