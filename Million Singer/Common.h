//
//  Common.h
//  Million Singer
//
//  Created by iBuildx-Macbook on 13/10/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Common : NSObject


+(void)setLoginResponseInDefaults :(NSDictionary *)loginResponserDictionary;
+(NSDictionary *)getLoginResponseInDefaults;
    
@end
