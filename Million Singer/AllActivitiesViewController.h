//
//  AllActivitiesViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 21/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllActivitiesViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *fundRaisingBtn;

@end
