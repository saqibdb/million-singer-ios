//
//  ServerCommunication.m
//  Million Singer
//
//  Created by iBuildx-Macbook on 13/10/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "ServerCommunication.h"
#import "SQBConstants.h"
#import "RegLogModel.h"
#import "Common.h"

@implementation ServerCommunication

+(void)loginWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kLoginReg] ;
    [manager POST:requestUrl parameters:dict progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}


+(void)VerifyOtpCodeWithCode :(NSString *)code  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kVerifyOtp] ;
    NSError *decodeError;
    RegLogModel *loginModel = [[RegLogModel alloc] initWithDictionary:[Common getLoginResponseInDefaults] error:&decodeError];
    if (decodeError) {
        responseBlock(nil , NO , decodeError);
        return;
    }
    NSLog(@"Session Key is %@" ,loginModel.session_key );
    NSDictionary *params = @{ @"otp_code": @"0000",
                              @"session_key": loginModel.session_key };
    [manager POST:requestUrl parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}

+(void)updateUsersProfile:(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kProfileUpdate] ;
    [manager POST:requestUrl parameters:dict progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}

+(void)userProfileView:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kUserProfileView] ;
    NSError *decodeError;
    RegLogModel *loginModel = [[RegLogModel alloc] initWithDictionary:[Common getLoginResponseInDefaults] error:&decodeError];
    if (decodeError) {
        responseBlock(nil , NO , decodeError);
        return;
    }
    NSLog(@"Session Key is %@" ,loginModel.session_key );
    NSDictionary *params = @{@"session_key": loginModel.session_key };
    [manager POST:requestUrl parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}

+(void)logout:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kLogout] ;
    NSError *decodeError;
    RegLogModel *loginModel = [[RegLogModel alloc] initWithDictionary:[Common getLoginResponseInDefaults] error:&decodeError];
    if (decodeError) {
        responseBlock(nil , NO , decodeError);
        return;
    }
    NSLog(@"Session Key is %@" ,loginModel.session_key );
    NSDictionary *params = @{@"session_key": loginModel.session_key };
    [manager POST:requestUrl parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}

+(void)userImage:(UIImage *)image andResponseBlock:(APIRequestResponseBlock)responseBlock {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",kBASE_URL , kUserImage] ;
    NSError *decodeError;
    RegLogModel *loginModel = [[RegLogModel alloc] initWithDictionary:[Common getLoginResponseInDefaults] error:&decodeError];
    if (decodeError) {
        responseBlock(nil , NO , decodeError);
        return;
    }
    NSLog(@"Session Key is %@" ,loginModel.session_key );
    NSDictionary *params = @{@"session_key": loginModel.session_key,
                             @"image": image
                             };
    [manager POST:requestUrl parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}

+(void) GetJsonResponsea :(APIRequestResponseBlock)responseBlock
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@" " forHTTPHeaderField:@"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1OTY4OTg3NDFmN2IwODQ0MjRkZDBhODEiLCJpYXQiOjE1MDI0NTE0NjksImV4cCI6MTUwNTA3OTQ2OX0.E6LEdQVKREaeVib9OWF8-zy5Z-ACrKDR3-RSn9DvFcM"];
    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"cache-contro"];
    [manager GET:@"http://54.254.206.27:9000/api/users/mostrecommended" parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Data uploaded = %@" , uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        responseBlock(nil , NO , error);
    }];
}
@end
