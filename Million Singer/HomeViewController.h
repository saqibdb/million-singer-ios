//
//  HomeViewController.h
//  Million Singer
//
//  Created by ibuildx on 8/3/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFBlurView.h"
#import <AKPickerView/AKPickerView.h>
#import <MIBadgeButton/MIBadgeButton.h>
#import "Slider.h"


@interface HomeViewController : UIViewController<UICollectionViewDelegate , UICollectionViewDataSource, AKPickerViewDataSource, AKPickerViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *titleText;
- (IBAction)moreAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *bottomText;
@property (weak, nonatomic) IBOutlet UICollectionView *countryCollectionView;
@property (weak, nonatomic) IBOutlet UIView *countriesView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceConstraint1;
@property (weak, nonatomic) IBOutlet AKPickerView *typePickerView;
@property (weak, nonatomic) IBOutlet MIBadgeButton *letterBtn;
@property (weak, nonatomic) IBOutlet MIBadgeButton *coinsBtn;
@property (weak, nonatomic) IBOutlet Slider *mainSlider;
@property (weak, nonatomic) IBOutlet UICollectionView *activitiesCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *singerCountryCollectionView;
@property (weak, nonatomic) IBOutlet UIView *headerGradientView;

- (IBAction)tickAction:(UIButton *)sender;
- (IBAction)countryAction:(UIButton *)sender;
- (IBAction)searchAction:(UIButton *)sender;
- (IBAction)rankAction:(UIButton *)sender;
- (IBAction)coinsAction:(MIBadgeButton *)sender;
- (IBAction)letterAction:(MIBadgeButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *gotoUserPageBtn;

- (IBAction)moreActivitiesAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *moreActivitiesBtn;
@property (weak, nonatomic) IBOutlet UIButton *moreBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryName;


@end
