//
//  MenuVideoEditViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 22/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "GPUImage.h"
#import <SCRecorder/SCRecorder.h>
#import "SAVideoRangeSlider.h"
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioToolbox.h>
@interface MenuVideoEditViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVPlayerViewControllerDelegate,SCAssetExportSessionDelegate,SAVideoRangeSliderDelegate,AVAudioPlayerDelegate>
- (IBAction)closeAction:(UIButton *)sender;
- (IBAction)nextAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *videoPreviewView;
@property (weak, nonatomic) IBOutlet UICollectionView *effectsCollectionView;

@property (weak, nonatomic) IBOutlet UIView *slideView;
@property (weak, nonatomic) IBOutlet UIView *cropView;
- (IBAction)cropDoneAction:(UIButton *)sender;
- (IBAction)showCropOptionsAction:(UIButton *)sender;
- (IBAction)playAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (nonatomic) AVPlayerViewController *playerViewController;
@property (strong, nonatomic) GPUImageSepiaFilter *cropFilter;
@property (strong, nonatomic) GPUImageMovieWriter *movieWriter;
@property (weak, nonatomic) IBOutlet UIView *trimViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblTrimTime;
- (IBAction)applyFiltersAction:(UIButton *)sender;
- (IBAction)clearAllFiltersAction:(UIButton *)sender;
@property (nonatomic, retain) AVAudioPlayer *soundPlayer;
- (IBAction)applyAudioFilter:(UIButton *)sender;
@property (strong) NSURL * videoUrl;


@end
