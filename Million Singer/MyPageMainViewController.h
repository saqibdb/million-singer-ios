//
//  MyPageMainViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 21/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPageMainViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>
- (IBAction)menueActions:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *menuBtns;
@property (weak, nonatomic) IBOutlet UIView *dynamicView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *allTheChipsView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *applyForAllBtn;
- (IBAction)backAction:(UIButton *)sender;

- (IBAction)commentAction:(UIButton *)sender;
- (IBAction)fansAction:(UIButton *)sender;
- (IBAction)atentionAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *responseBtn;


@end
