//
//  MyItemsTableViewCell.h
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyItemsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *itemTypeImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *quantityBtn;

@end
