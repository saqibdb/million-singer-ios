//
//  ViewController.h
//  Million Singer
//
//  Created by ibuildx on 8/2/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AKPickerView/AKPickerView.h>

@interface PreLoginViewController : UIViewController<AKPickerViewDataSource,AKPickerViewDelegate>

    
    
    
    @property (weak, nonatomic) IBOutlet UIImageView *mainLoog;
    @property (weak, nonatomic) IBOutlet UILabel *signInText;
    @property (weak, nonatomic) IBOutlet UILabel *agreementText;
    
@property (weak, nonatomic) IBOutlet UIView *gradientView;
    
    
    
@property (weak, nonatomic) IBOutlet AKPickerView *languagePickerView;

    @property (weak, nonatomic) IBOutlet UIButton *weChatBtn;
    @property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
    @property (weak, nonatomic) IBOutlet UIButton *twitterBtn;
    @property (weak, nonatomic) IBOutlet UIButton *googlePlusBtn;
    @property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
    @property (weak, nonatomic) IBOutlet UIButton *checkBtn;

    
- (IBAction)weChatAction:(UIButton *)sender;
- (IBAction)facebookAction:(UIButton *)sender;
- (IBAction)twitterAction:(UIButton *)sender;
- (IBAction)googlePlusAction:(UIButton *)sender;
- (IBAction)phoneAction:(UIButton *)sender;
- (IBAction)checkAction:(UIButton *)sender;
    
    
    
    
    
    
    
    
    
@end

