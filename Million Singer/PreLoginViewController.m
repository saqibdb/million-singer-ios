//
//  ViewController.m
//  Million Singer
//
//  Created by ibuildx on 8/2/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "PreLoginViewController.h"
#import "UIColor+Additions.h"
#import "Localisator.h"
#import <ChameleonFramework/Chameleon.h>
#define languages  [[NSArray alloc] initWithObjects:@"简体中文",@"繁體中文",@"English",@"日语", nil]


@interface PreLoginViewController (){
   // NSArray *languages;
    NSArray *arrayOfLanguages;
}

@end

@implementation PreLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    

    [self setupLanguagePicker];
    
    
    arrayOfLanguages = [[[Localisator sharedInstance]availableLanguagesArray] copy];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    [self.view layoutIfNeeded];
    UIColor *colorHigh = [UIColor add_colorWithRed255:177.0 green255:177.0 blue255:177.0 alpha255:59.0];
    UIColor *colorMedium = [UIColor add_colorWithRed255:174.0 green255:174.0 blue255:174.0 alpha255:44.0];
    UIColor *lowColor = [UIColor add_colorWithRed255:238.0 green255:238.0 blue255:238.0 alpha255:0.0];
    self.gradientView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:self.gradientView.frame andColors:[NSArray arrayWithObjects: lowColor, colorMedium, colorHigh, colorMedium, lowColor, nil]];
    [self.view layoutIfNeeded];
}

-(void)setupLanguagePicker {
    self.languagePickerView.delegate = self;
    self.languagePickerView.dataSource = self;
    self.languagePickerView.interitemSpacing = self.view.frame.size.width / 20;
    self.languagePickerView.textColor = [UIColor add_colorWithRGBHexString:@"B8B8B8"];
    self.languagePickerView.highlightedTextColor = [UIColor add_colorWithRGBHexString:@"FFFFFF"];
    [self.languagePickerView setFont:[UIFont fontWithName:@"PingFang TC" size:14]];
    [self.languagePickerView setHighlightedFont:[UIFont fontWithName:@"PingFang TC" size:17]];
}

-(void)viewWillDisappear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self setLocalizedString];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

    
#pragma mark - Notification methods
    
- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged]){
            [self setLocalizedString];
    }
}
    
-(void)setLocalizedString {
    [self.signInText setText:LOCALIZATION(@"sign_in")];
    [self.agreementText setText:LOCALIZATION(@"agreement")];
    [self.view layoutIfNeeded];
}

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView{
    return languages.count;
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item{
    return languages[item];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item{
    for (NSString *str in arrayOfLanguages) {
        //NSLog(@"STR = %@",str);
    }
}

- (IBAction)weChatAction:(UIButton *)sender {
    [self gotoCodeGeneration];
}
    
- (IBAction)facebookAction:(UIButton *)sender {
    [self gotoCodeGeneration];
}
    
- (IBAction)twitterAction:(UIButton *)sender {
    [self gotoCodeGeneration];
}
    
- (IBAction)googlePlusAction:(UIButton *)sender {
    [self gotoCodeGeneration];
}
    
- (IBAction)phoneAction:(UIButton *)sender {
    [self gotoCodeGeneration];
}
    
- (IBAction)checkAction:(UIButton *)sender {
    if (sender.tag) {
        sender.tag = 0;
        [sender setImage:[UIImage imageNamed:@"checkboxUnSelected"] forState:UIControlStateNormal];
    }
    else{
        sender.tag = 1;
        [sender setImage:[UIImage imageNamed:@"checkboxSelected"] forState:UIControlStateNormal];
    }
}

-(void)gotoCodeGeneration {
    [self performSegueWithIdentifier:@"ToCodeGeneration" sender:self];
}
    
    
    
@end
