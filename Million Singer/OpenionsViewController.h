//
//  OpenionsViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 08/09/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenionsViewController : UIViewController
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)leavEmailAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *leaveEmailBtn;

@end
