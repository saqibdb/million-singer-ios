//
//  ServiceViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end
