//
//  LiveVideoRecordViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 25/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "LiveVideoRecordViewController.h"

@interface LiveVideoRecordViewController ()

@end

@implementation LiveVideoRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.infoView setHidden:YES];
    [self.blackView.layer setCornerRadius:32];
    [self.homeBtn.layer setCornerRadius:8];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeAction:(UIButton *)sender {
    [self.infoView setHidden:YES];

}
- (IBAction)recordAction:(UIButton *)sender {
    [self.infoView setHidden:NO];

}
@end
