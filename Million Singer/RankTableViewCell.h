//
//  RankTableViewCell.h
//  Million Singer
//
//  Created by MACBOOK PRO on 12/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RankTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *rankTypeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *totalFansCount;
@property (weak, nonatomic) IBOutlet UIButton *attentionBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *lblRank;

@property (weak, nonatomic) IBOutlet UILabel *secondUserName;



@end
