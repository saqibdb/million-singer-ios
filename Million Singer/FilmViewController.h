//
//  FilmViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 25/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
@interface FilmViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,AVPlayerViewControllerDelegate,AVPlayerViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)nextAction:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *videoPreviewView;
@property (nonatomic) AVPlayerViewController *playerViewController;
- (IBAction)playAction:(UIButton *)sender;


@end
