
//
//  PhoneVerificationViewController.m
//  Million Singer
//
//  Created by Ali Apple  on 8/2/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "PhoneVerificationViewController.h"


#import "UIColor+Additions.h"
#import "Localisator.h"
#import <ChameleonFramework/Chameleon.h>
#import "PCCPViewController.h"
#import "ServerCommunication.h"
#import "RegLogModel.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Common.h"


@interface PhoneVerificationViewController ()

@end

@implementation PhoneVerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [self setLocalizedString];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.view layoutIfNeeded];
    //self.view.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.view.frame andColors:[NSArray arrayWithObjects:[UIColor add_colorWithRed255:28.0 green255:37.0 blue255:136.0 alpha255:100.0],[UIColor add_colorWithRed255:27.0 green255:67.0 blue255:34.0 alpha255:100.0], nil]];
    [self.view layoutIfNeeded];
}

-(void)viewWillDisappear:(BOOL)animated{
    //self.view.backgroundColor = [UIColor blackColor];
    [self.navigationItem setTitle:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Notification methods

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged]){
        [self setLocalizedString];
    }
}

-(void)setLocalizedString {
    [self.navigationItem setTitle:LOCALIZATION(@"sign_in_title")];
    //[self.countryText setText:LOCALIZATION(@"hong_kong")];
    [self.phoneNumberTextF setPlaceholder:LOCALIZATION(@"enter_phone_number")];
    [self.verificationCodeBtn setTitle:LOCALIZATION(@"get_verification_code") forState:UIControlStateNormal];
    
    
    NSString *firstDescription = LOCALIZATION(@"verification_description1");
    NSString *secondDescription = LOCALIZATION(@"verification_description2");

    NSString *combinedDescription = [NSString stringWithFormat:@"%@ %@",firstDescription, secondDescription];

    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:combinedDescription];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,firstDescription.length)];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor add_colorWithRGBHexString:@"00B2FB"] range:NSMakeRange(firstDescription.length ,combinedDescription.length - firstDescription.length)];

   
    if (string) {
        self.verificationDescriptionText.attributedText = string;
    }
    [self.view layoutIfNeeded];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)countryAction:(UIButton *)sender {
      PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        self.countryText.text = [NSString stringWithFormat:@"%@",[countryDic valueForKey:@"country_en"]];
        self.countryCodeText.text = [NSString stringWithFormat:@"+%@",[countryDic valueForKey:@"phone_code"]];
     }];
    [vc setIsUsingChinese:NO];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
}

- (IBAction)verificationCodeAction:(UIButton *)sender {
//    [self.view endEditing:YES];
//    if (!self.phoneNumberTextF.text.length) {
//        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:@"Phone Number Cannot be Empty." andCancelButton:NO forAlertType:AlertInfo];
//        [alert show];
//        return;
//    }
//    [SVProgressHUD showWithStatus:@"Checking Credentials..."];
//    NSString *fullNumber = [NSString stringWithFormat:@"%@%@",self.countryCodeText.text,self.phoneNumberTextF.text];
//    NSDictionary *params = @{ @"registration_type": @"1",
//                              @"number": fullNumber,
//                              @"device_type": @"2",
//                              @"device_id": @"12345" };
//    [ServerCommunication loginWithDictionary:params AndResponseBlock:^(id object, BOOL status, NSError *error) {
//        [SVProgressHUD dismiss];
//        if (!error) {
//            NSError *decodeError;
//            RegLogModel *logInResponse = [[RegLogModel alloc] initWithDictionary:object error:&decodeError];
//            if (!decodeError) {
//                NSLog(@"Server Key is %@" , logInResponse.session_key);
//                [Common setLoginResponseInDefaults:[logInResponse toDictionary]];
//                if (!logInResponse.verify_no) {
//                    [self verificationCodeAction:nil];
//                }
//                else{
                    [self performSegueWithIdentifier:@"ToVerificationCode" sender:self];
//                }
//            }
//            else{
//                NSLog(@"Error is %@" , decodeError.description);
//            }
//        }
//        else{
//            NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
//            if (!ErrorResponse.length) {
//                ErrorResponse = error.localizedDescription;
//            }
//            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:ErrorResponse andCancelButton:NO forAlertType:AlertFailure];
//            [alert show];
//        }
//    }];
}
@end
