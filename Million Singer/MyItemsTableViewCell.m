//
//  MyItemsTableViewCell.m
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "MyItemsTableViewCell.h"

@implementation MyItemsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.quantityBtn.layer setCornerRadius:12];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
