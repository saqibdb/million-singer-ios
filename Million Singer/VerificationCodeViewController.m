//
//  VerificationCodeViewController.m
//  Million Singer
//
//  Created by Ali Apple  on 8/4/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "VerificationCodeViewController.h"
#import "UIColor+Additions.h"
#import "Localisator.h"
#import "UIViewController+CWPopup.h"
#import "GoldCoinsPopUpView.h"
#import "BindCellPopUpView.h"
#import "ServerCommunication.h"
#import "RegLogModel.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Common.h"

@interface VerificationCodeViewController ()

@end

@implementation VerificationCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.verificationCodeText.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.useBlurForPopup = YES;
    self.verificationCodeText.text = @"123456";

}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self setLocalizedString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notification methods

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:kNotificationLanguageChanged]){
        [self setLocalizedString];
    }
}

-(void)setLocalizedString {
    [self.navigationItem setTitle:LOCALIZATION(@"verification_title")];
    
    [self.verificationCodeText setPlaceholder:LOCALIZATION(@"enter_verification_code")];
    [self.nextStepBtn setTitle:LOCALIZATION(@"next_step") forState:UIControlStateNormal];
    [self.view layoutIfNeeded];
}



- (void)openCoinsPopup {
    GoldCoinsPopUpView *goldCoinsPopUpView = [[GoldCoinsPopUpView alloc] initWithNibName:@"GoldCoinsPopUpView" bundle:nil];
    goldCoinsPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y/2, self.view.frame.size.width- 100, (self.view.frame.size.width- 100) * 0.68 );
    [goldCoinsPopUpView.view layoutIfNeeded];
    goldCoinsPopUpView.topSpaceConstraint.constant = (goldCoinsPopUpView.topBorderView.frame.size.width / 2) * -1;
    goldCoinsPopUpView.topBorderView.cornerRadius = goldCoinsPopUpView.topBorderView.frame.size.width / 2;
    [goldCoinsPopUpView.view layoutIfNeeded];
    [self presentPopupViewController:goldCoinsPopUpView animated:YES completion:^(void) {
    [goldCoinsPopUpView.shareBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)cellBindPopup {
    BindCellPopUpView *bindCellPopUpView = [[BindCellPopUpView alloc] initWithNibName:@"BindCellPopUpView" bundle:nil];
    bindCellPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y/2, self.view.frame.size.width- 100, (self.view.frame.size.width- 100) * 0.68 );
    [bindCellPopUpView.view layoutIfNeeded];
    bindCellPopUpView.topSpaceConstraint.constant = (bindCellPopUpView.topBorderView.frame.size.width / 2) * -1;
    bindCellPopUpView.topBorderView.cornerRadius = bindCellPopUpView.topBorderView.frame.size.width / 2;
    [bindCellPopUpView.view layoutIfNeeded];
    [self presentPopupViewController:bindCellPopUpView animated:YES completion:^(void) {
        [bindCellPopUpView.shareBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
        }];
    }
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= 6 || returnKey;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextStepAction:(UIButton *)sender {
//    if (self.verificationCodeText.text.length == 6) {
//        
//        [SVProgressHUD showWithStatus:@"Checking Code..."];
//        
//        [ServerCommunication VerifyOtpCodeWithCode:self.verificationCodeText.text AndResponseBlock:^(id object, BOOL status, NSError *error) {
//            [SVProgressHUD dismiss];
//            if (!error) {
//                NSDictionary *responseDictionary = (NSDictionary *)object;
//                if ([responseDictionary objectForKey:@"error"] && ([[responseDictionary objectForKey:@"msg"]isEqualToString:@"Success"])) {
                    [self performSegueWithIdentifier:@"ToNextStoryboard" sender:self];
//
//                }
//                else{
//                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:[responseDictionary objectForKey:@"msg"] andCancelButton:NO forAlertType:AlertFailure];
//                    [alert show];
//                }
//            }
//            else{
//                NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
//                if (!ErrorResponse.length) {
//                    ErrorResponse = error.localizedDescription;
//                }
//                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:ErrorResponse andCancelButton:NO forAlertType:AlertFailure];
//                [alert show];
//            }
//        }];
//        
//        
//        
//    }
//    else
//    {
//        UIAlertController * alert = [UIAlertController
//                                     alertControllerWithTitle:@""
//                                     message:@"Please enter 6 digits verification code"
//                                     preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* OkBtn = [UIAlertAction
//                                actionWithTitle:@"Ok"
//                                style:UIAlertActionStyleDefault
//                                handler:^(UIAlertAction * action) {
//                                    
//                                    //Handle your yes please button action here
//                                }];
//        [alert addAction:OkBtn];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
   
}
@end
