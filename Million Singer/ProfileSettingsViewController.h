//
//  ProfileSettingsViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 17/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileSettingsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)fanAndSingerActions:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *fansAndSingersBtns;
@property (weak, nonatomic) IBOutlet UILabel *lblPersonal;
@property (weak, nonatomic) IBOutlet UILabel *lblAttentions;
@property (weak, nonatomic) IBOutlet UILabel *lblFans;

@end
