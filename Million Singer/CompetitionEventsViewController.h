//
//  CompetitionEventsViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 12/09/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompetitionEventsViewController : UIViewController<UITableViewDelegate , UITableViewDataSource>
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)watchLiveAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *watchLiveBtn;

@end
