//
//  CenterMenuViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 31/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterMenuViewController : UIViewController
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *mainViews;
- (IBAction)recordingAction:(UIButton *)sender;
- (IBAction)playAction:(UIButton *)sender;
- (IBAction)explorerAction:(UIButton *)sender;
- (IBAction)folderAction:(UIButton *)sender;
- (IBAction)mainBtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblVideo;
@property (weak, nonatomic) IBOutlet UILabel *lblLive;
@property (weak, nonatomic) IBOutlet UILabel *lblFilm;
@property (weak, nonatomic) IBOutlet UILabel *lblDraft;

@end
