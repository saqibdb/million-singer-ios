//
//  SearchViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 12/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "SearchViewController.h"
#import "SingersCountryCollectionViewCell.h"
#import "Localisator.h"

@interface SearchViewController ()
{
    int selectedCatagoryTag;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self settingLayouts];
    [self setButtonTitles];
   
    // Do any additional setup after loading the view.
}
-(void)setButtonTitles{
    for (UIButton *btn in self.searchCatagoryButtons) {
        if (btn.tag == 0) {
            [btn setTitle:LOCALIZATION(@"character") forState:UIControlStateNormal];
        }
        else if (btn.tag == 1) {
                [btn setTitle:LOCALIZATION(@"label") forState:UIControlStateNormal];
        }
        else if (btn.tag == 2) {
            [btn setTitle:LOCALIZATION(@"song") forState:UIControlStateNormal];
        }
        else if (btn.tag == 3) {
            [btn setTitle:LOCALIZATION(@"film") forState:UIControlStateNormal];
        }
        else if (btn.tag == 4) {
            [btn setTitle:LOCALIZATION(@"live") forState:UIControlStateNormal];
        }
    }
    
    //global and area buttons
    
    for (UIButton *btn in self.globalAndAreaButtons) {
        if (btn.tag == 0) {
            [btn setTitle:LOCALIZATION(@"global") forState:UIControlStateNormal];
        }
        else if (btn.tag == 1) {
            [btn setTitle:LOCALIZATION(@"area") forState:UIControlStateNormal];
        }
    }
    
    [self.returnBtn setTitle:LOCALIZATION(@"return") forState:UIControlStateNormal];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)settingLayouts{
    [self.view layoutIfNeeded];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView setHidden:YES];
    [self.tableView setHidden:NO];
    [self.searchViewContainer.layer setCornerRadius:6];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - tableViewDelegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"searchCell";
    SearchTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[SearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:MyIdentifier];
    }
    
    switch (selectedCatagoryTag) {
        case 0:
        {
            [cell.profileImage setImage:[UIImage imageNamed:@"MaryProfile"]];
            cell.userName.text = @"Mary";
            cell.messageText.text = @"nice to meet you";
            [cell.countryImage setHidden:NO];
            [cell.profileImage setHidden:NO];
            [cell.iconImage setHidden:YES];
            
            break;
        }
        case 1:
        {
            
            [cell.iconImage setImage:[UIImage imageNamed:@"tagIcon"]];
            cell.userName.text = @"# A";
            cell.messageText.text = @"10,000,000條短片";
            [cell.countryImage setHidden:YES];
            [cell.profileImage setHidden:YES];
            [cell.iconImage setHidden:NO];
            
            break;
        }
        case 2:
        {
            
            [cell.iconImage setImage:[UIImage imageNamed:@"musicIcon-1"]];
            cell.userName.text = @"小小的宇宙";
            cell.messageText.text = @"10,000,000條短片";
            [cell.countryImage setHidden:YES];
            [cell.profileImage setHidden:YES];
            [cell.iconImage setHidden:NO];
            
            break;
        }
        default:
            break;
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - collectionView Delegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 10.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 9;}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SingersCountryCollectionViewCell *cell=(SingersCountryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SingersCountryCollectionViewCell" forIndexPath:indexPath];
    [cell layoutIfNeeded];
    cell.profilePictureBorderView.cornerRadius = cell.profilePictureBorderView.frame.size.width / 2;
    cell.countryBorderView.cornerRadius = cell.countryBorderView.frame.size.width / 2;
    cell.songNameBorderView.cornerRadius = cell.songNameBorderView.frame.size.height / 2;
    cell.backgroundColor = [UIColor clearColor];
    [cell layoutIfNeeded];
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    float cellWitdth = (collectionView.frame.size.width / 2) - 5;
//    float cellHeight = (255.0 / 218.0) * cellWitdth;
    float cellHeight = 255.0;

    return CGSizeMake(cellWitdth, cellHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - Actions
- (IBAction)SearchCancelAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)globalAndAreaActions:(UIButton *)sender {
}

- (IBAction)searchCatagoryActions:(UIButton *)sender {
    
    if (sender.tag <=2) {
        [self.collectionView setHidden:YES];
        [self.tableView setHidden:NO];
        selectedCatagoryTag = (int)sender.tag;
        [self.tableView reloadData];
    }
    else
    {
        [self.collectionView setHidden:NO];
        [self.tableView setHidden:YES];
        selectedCatagoryTag = (int)sender.tag;
        [self.collectionView reloadData];
        
        
    }
    
    UIColor *myThemeColor = [UIColor colorWithRed:95/255.0 green:121/255.0 blue:133/255.0 alpha:1];
    for (UIButton *btn in self.searchCatagoryButtons) {
        if (btn.tag == sender.tag) {
            
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else{
            
            [btn setTitleColor:myThemeColor forState:UIControlStateNormal];
        }
    }
}
@end
