//
//  EventsTableViewCell.m
//  Million Singer
//
//  Created by MACBOOK PRO on 16/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "EventsTableViewCell.h"

@implementation EventsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.watchLiveBtn.layer setCornerRadius:5];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
