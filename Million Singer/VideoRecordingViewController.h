//
//  VideoRecordingViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 24/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WowzaGoCoderSDK/WowzaGoCoderSDK.h>







@interface VideoRecordingViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)BottomMenueActions:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *editorView;
- (IBAction)showEditorAction:(UIButton *)sender;
- (IBAction)refreshAction:(id)sender;
- (IBAction)closeAction:(UIButton *)sender;
- (IBAction)StartRecordingAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *editorViewBottom;
@property (strong, nonatomic) IBOutlet UIView *shareContainerVew;
@property (weak, nonatomic) IBOutlet UIButton *startLiveBtn;
- (IBAction)startLiveAction:(UIButton *)sender;
- (IBAction)showShareViewAction:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *thumbnailView;

@property (strong, nonatomic) IBOutlet UIView *streamAnimationView;


+ (void) showAlertWithTitle:(NSString *)title status:(WZStatus *)status presenter:(UIViewController *)presenter;
+ (void) showAlertWithTitle:(NSString *)title error:(NSError *)error presenter:(UIViewController *)presenter;
+ (void) presentAlert:(nullable NSString *)title message:(nullable NSString *)message presenter:(nonnull UIViewController *)presenter;

@end
