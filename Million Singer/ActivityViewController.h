//
//  ActivityViewController.h
//  Million Singer
//
//  Created by ibuildx on 8/11/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMWheelControl.h"
#import <CCMBorderView/CCMBorderView.h>


@interface ActivityViewController : UIViewController<SMWheelControlDelegate, SMWheelControlDataSource>



@property (weak, nonatomic) IBOutlet SMWheelControl *wheelView;
@property (weak, nonatomic) IBOutlet CCMBorderView *wheelButtonVeiw;
@property (weak, nonatomic) IBOutlet UIButton *drawNowBtm;
- (IBAction)drawNowAction:(UIButton *)sender;

@end
