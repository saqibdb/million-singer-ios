//
//  LiveVideoRecordViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 25/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveVideoRecordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *blackView;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)homeAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *infoView;
- (IBAction)recordAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *homeBtn;

@end
