//
//  ApplyForAllViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 22/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplyForAllViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *applyForAllBtn;
- (IBAction)backAction:(UIButton *)sender;

@end
