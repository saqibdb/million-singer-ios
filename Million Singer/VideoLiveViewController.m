//
//  VideoLiveViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 29/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "VideoLiveViewController.h"
#import "CommentsTableViewCell.h"
#import "GiftsCollectionViewCell.h"
@interface VideoLiveViewController ()

@end

@implementation VideoLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.collectionVew.dataSource = self;
    self.collectionVew.delegate = self;
    [self settingLayouts];
   
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - collection Delegate
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    
//    return 10.0;
//}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 9;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GiftsCollectionViewCell *cell=(GiftsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"giftsCell" forIndexPath:indexPath];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 4; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth + 35);
    return size;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}


#pragma mark - tableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"commentsCell";
    CommentsTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[CommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:MyIdentifier];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - Method and actions
-(void)settingLayouts{
    [self.commentsView setHidden:YES];
    [self.giftsView setHidden:YES];
    [self.sharePopUpView setHidden:YES];
    [self.userInfoView setHidden:YES];
    [self.profitView setHidden:YES];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    doubleTap.delegate = self;
    [self.view addGestureRecognizer:doubleTap];
    
    [self.txtComment.layer setCornerRadius:13];
}

-(void)viewTapped:(UIGestureRecognizer *)sender {

    [_commentsView setHidden:YES];
    [_giftsView setHidden:YES];

}
- (IBAction)bottomMenuActions:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            [_commentsView setHidden:NO];
            [_giftsView setHidden:YES];
            [self.sharePopUpView setHidden:YES];
            [self.userInfoView setHidden:YES];
            break;
        }
        case 1:
        {
            [self.sharePopUpView setHidden:NO];
            [self.userInfoView setHidden:YES];
            [_commentsView setHidden:YES];
            [_giftsView setHidden:YES];
            break;
        }
        case 2:
        {
            break;
        }
        case 3:
        {
            [_commentsView setHidden:YES];
            [_giftsView setHidden:NO];
            break;
        }
        case 4:
        {
            break;
        }
        default:
            break;
    }
}
- (IBAction)determineAction:(UIButton *)sender {
    [self.sharePopUpView setHidden:YES];

}

- (IBAction)attentionAction:(UIButton *)sender {
    [self.userInfoView setHidden:YES];
}

- (IBAction)showUserInfoAction:(UIButton *)sender {
    
    [self.userInfoView setHidden:NO];

}


- (IBAction)redPopUpDetermineAction:(UIButton *)sender {
    [self.profitView setHidden:YES];

}

- (IBAction)showProfitViewAction:(UIButton *)sender {
    [self.profitView setHidden:NO];
    [self.giftsView setHidden:YES];

}
@end
