//
//  SingersCountryCollectionViewCell.h
//  Million Singer
//
//  Created by Ali Apple  on 8/9/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>

@interface SingersCountryCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;

@property (weak, nonatomic) IBOutlet UILabel *singerName;
@property (weak, nonatomic) IBOutlet UILabel *singerSubtitle;
@property (weak, nonatomic) IBOutlet CCMBorderView *profilePictureBorderView;
@property (weak, nonatomic) IBOutlet CCMBorderView *countryBorderView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;
@property (weak, nonatomic) IBOutlet UIImageView *countryImageview;
@property (weak, nonatomic) IBOutlet CCMBorderView *songNameBorderView;
@property (weak, nonatomic) IBOutlet UILabel *songNameText;
@property (weak, nonatomic) IBOutlet UIButton *goToUserPageBtn;


@end
