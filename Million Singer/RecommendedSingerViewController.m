//
//  RecommendedSingerViewController.m
//  Million Singer
//
//  Created by ibuildx on 8/3/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "RecommendedSingerViewController.h"
#import "RecommendedSingerCollectionViewCell.h"
#import "Localisator.h"

@interface RecommendedSingerViewController ()

@end

@implementation RecommendedSingerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.singerCollectionView.delegate = self;
    self.singerCollectionView.dataSource = self;
    
    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"X"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(skipTapped)];
    self.navigationItem.rightBarButtonItem = flipButton;
    

}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.title = LOCALIZATION(@"recomended_singers");
}
-(void)viewDidAppear:(BOOL)animated{
    //[self performSegueWithIdentifier:@"ToHome" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 18;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RecommendedSingerCollectionViewCell *cell=(RecommendedSingerCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"RecommendedSingerCollectionViewCell" forIndexPath:indexPath];
    [cell layoutIfNeeded];
    cell.profileBorderView.cornerRadius = cell.profileBorderView.frame.size.width / 2;
    return cell;

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float cellWitdth = (self.view.frame.size.width / 3) - 10;
    float cellHeight = (215.0 / 138.0) * cellWitdth;
    return CGSizeMake(cellWitdth, cellHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"ToHome" sender:self];
}

-(void)skipTapped{
    [self performSegueWithIdentifier:@"ToHome" sender:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
