//
//  SearchViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 12/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchTableViewCell.h"
@interface SearchViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *searchViewContainer;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchText;
- (IBAction)SearchCancelAction:(UIButton *)sender;
- (IBAction)globalAndAreaActions:(UIButton *)sender;
- (IBAction)searchCatagoryActions:(UIButton *)sender;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *searchCatagoryButtons;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *globalAndAreaButtons;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *returnBtn;


@end
