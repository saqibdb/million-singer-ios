//
//  RechargeTableViewCell.h
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RechargeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *giveAwayAmountLable;
@property (weak, nonatomic) IBOutlet UIButton *dollorsBtn;

@end
