//
//  Common.m
//  Million Singer
//
//  Created by iBuildx-Macbook on 13/10/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "Common.h"
#import "SQBConstants.h"

@implementation Common



+(void)setLoginResponseInDefaults :(NSDictionary *)loginResponserDictionary{
    [[NSUserDefaults standardUserDefaults] setObject:loginResponserDictionary forKey:kLoggedInKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSDictionary *)getLoginResponseInDefaults{
    NSDictionary * myDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLoggedInKey];
    if (myDictionary) {
        return myDictionary;
    }
    else{
        return nil;
    }
}




@end
