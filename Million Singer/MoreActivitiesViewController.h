//
//  MoreActivitiesViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 19/09/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreActivitiesViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
- (IBAction)backAction:(UIButton *)sender;

@end
