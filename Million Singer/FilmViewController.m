//
//  FilmViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 25/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "FilmViewController.h"
#import "MenuVideoEditViewController.h"
#import "AMSmoothAlertView.h"
@interface FilmViewController (){
    NSMutableArray *AllVideosURLsArray;
    NSURL *urlToSend;
}

@end

@implementation FilmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    AllVideosURLsArray = [[NSMutableArray alloc]init];
    [self fetchAllVideosFromUserDefaults];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchAllVideosFromUserDefaults{
   
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError *error;
    NSArray *directoryContent = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory error:&error];
   
    for (NSString *subPath in directoryContent) {
        if ([subPath containsString:@"savedVideo.mp4"]) {
            NSString  *fullPath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,subPath];
            NSURL *url = [NSURL fileURLWithPath:fullPath];
            [AllVideosURLsArray addObject:url];
        }
    }
    
    if (AllVideosURLsArray.count > 0) {
      NSURL *url =   [AllVideosURLsArray objectAtIndex:0];
      urlToSend = url;
      [self setUpVideoForPreview:url];
    }
    
    [self.collectionView reloadData];
}
#pragma mark - collection Delegate


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return AllVideosURLsArray.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell=(UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"myfilmsCollectionCell" forIndexPath:indexPath];
   
    
    NSURL *videoURl = [AllVideosURLsArray objectAtIndex:indexPath.row];
    UIImageView *thumbnailImage = (UIImageView *)[cell viewWithTag:100];
    [thumbnailImage setImage:[self testGenerateThumbNailDataWithVideo:videoURl]];
   
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = (screenWidth / 2.0) -2; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth);
    return size;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSURL *url = [AllVideosURLsArray objectAtIndex:indexPath.row];
    [self.playerViewController.view removeFromSuperview];
    self.playerViewController = nil;
    [self setUpVideoForPreview:url];
}


-(void)setUpVideoForPreview:(NSURL *)url{
    
    if (!_playerViewController) {
        
        _playerViewController = [[AVPlayerViewController alloc] init];
        _playerViewController.delegate = self;
        _playerViewController.view.frame = CGRectMake(0, 0, self.videoPreviewView.frame.size.width, self.videoPreviewView.frame.size.height) ;
        _playerViewController.showsPlaybackControls = YES;
        // First create an AVPlayerItem
        AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:url];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
        _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
        [self.videoPreviewView addSubview:_playerViewController.view];
        
        
    }
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}
- (UIImage*)testGenerateThumbNailDataWithVideo:(NSURL *)videoURL {
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *currentImg = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    
    return currentImg;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controll
}
*/

- (IBAction)nextAction:(UIButton *)sender {
    if (!urlToSend) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"please select video" andCancelButton:NO forAlertType:AlertSuccess];
        [alert show];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MenueVideo" bundle:nil];
        MenuVideoEditViewController *viewController = (MenuVideoEditViewController *)[storyboard instantiateViewControllerWithIdentifier:@"MenuVideoEditViewController"];
        // [self presentViewController:viewController animated:YES completion:nil];
        viewController.videoUrl = urlToSend;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)playAction:(UIButton *)sender {
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    videoPicker.delegate = self;
    videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    videoPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4] ;
    UIView *v = [self.view viewWithTag:88];
    v.hidden = YES;
    videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:videoPicker animated:YES completion:nil];
}

#pragma mark - Optional EasyTableView delegate methods for section headers and footers
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    urlToSend = videoURL;
    [self.playerViewController.view removeFromSuperview];
    self.playerViewController = nil;
    [self setUpVideoForPreview:videoURL];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
@end
