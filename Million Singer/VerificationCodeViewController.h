//
//  VerificationCodeViewController.h
//  Million Singer
//
//  Created by Ali Apple  on 8/4/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerificationCodeViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeText;
@property (weak, nonatomic) IBOutlet UIButton *nextStepBtn;


- (IBAction)nextStepAction:(UIButton *)sender;


@end
