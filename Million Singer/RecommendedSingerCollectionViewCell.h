//
//  RecommendedSingerCollectionViewCell.h
//  Million Singer
//
//  Created by ibuildx on 8/3/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>
@interface RecommendedSingerCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet CCMBorderView *profileBorderView;

@property (weak, nonatomic) IBOutlet UIImageView *hongkongIcon;
@property (weak, nonatomic) IBOutlet UILabel *nameText;
@property (weak, nonatomic) IBOutlet UILabel *fansTExt;


@property (weak, nonatomic) IBOutlet UIButton *attentionBtm;




@end
