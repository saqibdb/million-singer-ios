//
//  EditProfileViewController.m
//  Million Singer
//
//  Created by MACBOOK PRO on 18/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import "EditProfileViewController.h"
#import "ServerCommunication.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Common.h"
#import "UserModel.h"

@interface EditProfileViewController ()
{
    UIImage *selectedImage;
}
@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [self.changeProfileOptionsView setHidden:YES];
    [self.shareView setHidden:YES];
    [self fetchUsersData];
    [self.userProfileImage.layer setCornerRadius:self.userProfileImage.frame.size.height/2];
    self.userProfileImage.clipsToBounds = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)changeProfileOptionsCancelAction:(UIButton *)sender {
    [self.changeProfileOptionsView setHidden:YES];
    
}

- (IBAction)cangeProfileAction:(UIButton *)sender {
    [self.changeProfileOptionsView setHidden:NO];
}
- (IBAction)canceShareAction:(UIButton *)sender {
    [self.shareView setHidden:YES];
}

- (IBAction)shareAction:(UIButton *)sender {
    [self.shareView setHidden:NO];
    
}
- (IBAction)saveProfileAction:(UIButton *)sender {
    
    if (!self.txtNickName.text.length || !self.txtIntro.text.length || !self.txtGender.text.length || !self.txtDOB.text.length || !self.txtHoroscope.text.length || !self.txtLocation.text.length) {
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:@"All fields are required" andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }
    else
    {
        [SVProgressHUD show];
        NSDictionary *userInfo = [Common getLoginResponseInDefaults];
        NSString *sessionKey = [userInfo valueForKey:@"session_key"];
        NSDictionary *params = @{ @"session_key": sessionKey,
                                  @"nick_name": self.txtNickName.text,
                                  @"intro": self.txtIntro.text,
                                  @"gender": @1,
                                  @"dob": self.txtDOB.text,
                                  @"horoscope": self.txtHoroscope.text,
                                  @"location": self.txtLocation.text,
                                  @"age": @27};
        [ServerCommunication updateUsersProfile:params AndResponseBlock:^(id object, BOOL status, NSError *error) {
            [SVProgressHUD dismiss];
            if (!error) {
                NSDictionary *responseDictionary = (NSDictionary *)object;
                if ([responseDictionary objectForKey:@"error"] && ([[responseDictionary objectForKey:@"msg"]isEqualToString:@"Success"])) {
                
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"User updated" andCancelButton:NO forAlertType:AlertSuccess];
                    [alert show];
                
                }
             }
            else{
                NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                if (!ErrorResponse.length) {
                    ErrorResponse = error.localizedDescription;
                }
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:ErrorResponse andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
        }];
        
    }
}

-(IBAction)takePicORChooseAction:(UIButton *)sender {
    if (sender.tag == 0) {
        [self showImagePicker:UIImagePickerControllerSourceTypeCamera sender:nil];
    }
    else if(sender.tag == 1)
    {
        [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary sender:nil];
    }
    else
    {
        [self.changeProfileOptionsView setHidden:YES];
    }
}

- (void)showImagePicker:(UIImagePickerControllerSourceType)sourceType sender:(id)sender {
    
    UIImagePickerController *controler = [[UIImagePickerController alloc] init];
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        return;
    }
    controler.sourceType = sourceType;
    controler.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        
        [self presentViewController:controler animated:YES completion:nil];
        
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Picker returned successfully.");
    
    NSURL *mediaUrl;
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    if (mediaUrl == nil) {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil) {
            selectedImage= (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
           [self.userProfileImage setImage:selectedImage];
            [self.changeProfileOptionsView setHidden:YES];
            
        }
        else {
            NSLog(@"Edited image picked.");
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
}


-(void)fetchUsersData{
    [SVProgressHUD show];
    [ServerCommunication userProfileView:^(id object, BOOL status, NSError *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSDictionary *responseDictionary = (NSDictionary *)object;
            if ([responseDictionary objectForKey:@"error"] && ([[responseDictionary objectForKey:@"msg"]isEqualToString:@"Success"])) {
               
                //assign data here;
                NSDictionary *userDict = [[responseDictionary valueForKeyPath:@"users"] objectAtIndex:0];
                NSError *decodeError;
                UserModel *user = [[UserModel alloc]initWithDictionary:userDict error:&decodeError];
                if (decodeError) {
                    NSLog(@"%@",decodeError.description);
                }
                else
                {
                    [self setUserDataInUI:user];
                }
            }
            else{
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:[responseDictionary objectForKey:@"msg"] andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
        }
        else{
            NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            if (!ErrorResponse.length) {
                ErrorResponse = error.localizedDescription;
            }
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:ErrorResponse andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
    }];

}

-(void)uploadUserImage{

   
    if (selectedImage) {
     [ServerCommunication userImage:selectedImage andResponseBlock:^(id object, BOOL status, NSError *error){
        [SVProgressHUD dismiss];
        if (!error) {
            NSDictionary *responseDictionary = (NSDictionary *)object;
            if ([responseDictionary objectForKey:@"error"] && ([[responseDictionary objectForKey:@"msg"]isEqualToString:@"Success"])) {
                
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"profile upadated" andCancelButton:NO forAlertType:AlertSuccess];
                [alert show];
            }
        }
        else{
            NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            if (!ErrorResponse.length) {
                ErrorResponse = error.localizedDescription;
            }
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:ErrorResponse andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
    }];
        }
}

-(void)setUserDataInUI:(UserModel *)user{


    if (![user.nick_name isEqual:[NSNull null]]) {
        self.txtNickName.text = user.nick_name;
    }
    else
    {
        self.txtNickName.text = @"";
    }
    if (![user.intro isEqual:[NSNull null]]) {
        self.txtIntro.text = user.intro;
    }
    else
    {
        self.txtIntro.text = @"";
    }
    if (![user.gender isEqual:[NSNull null]]) {
        self.txtGender.text = user.gender;
    }
    else
    {
        self.txtGender.text = @"";
    }
    if (![user.dob isEqual:[NSNull null]]) {
        self.txtDOB.text = user.dob;
    }
    else
    {
        self.txtDOB.text = @"";
    }
    if (![user.location isEqual:[NSNull null]]) {
       self.txtLocation.text = user.location;
    }
    else
    {
        self.txtLocation.text = @"";
    }
    self.txtHoroscope.text = @"";
}
@end
