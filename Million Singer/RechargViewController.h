//
//  RechargViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RechargViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *availableGoldLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)getFreeGoldCoinsAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *getFreeGoldCoinsBtn;

@property (weak, nonatomic) IBOutlet UILabel *rechargeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGoldBalence;
@property (weak, nonatomic) IBOutlet UILabel *lblWantCoins;
@property (weak, nonatomic) IBOutlet UIButton *btnDoBuisness;


@end
