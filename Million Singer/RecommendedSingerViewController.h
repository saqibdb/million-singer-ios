//
//  RecommendedSingerViewController.h
//  Million Singer
//
//  Created by ibuildx on 8/3/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendedSingerViewController : UIViewController<UICollectionViewDelegate , UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *singerCollectionView;




@end
