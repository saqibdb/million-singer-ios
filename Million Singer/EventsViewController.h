//
//  EventsViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 16/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Daysquare.h"

@interface EventsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)eventsAction:(UIButton *)sender;
- (IBAction)eventVideosAction:(UIButton *)sender;
- (IBAction)searchAction:(UIButton *)sender;
- (IBAction)rankAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rankBtnTrailingconstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet DAYCalendarView *calenderView;
@property (weak, nonatomic) IBOutlet UIButton *activityBtn;
@property (weak, nonatomic) IBOutlet UIButton *filmBtn;

@end
