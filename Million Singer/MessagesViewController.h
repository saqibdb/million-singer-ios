//
//  MessagesViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)autoReplyAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)topBarActions:(UIButton *)sender;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *topBarBtns;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoReply;


@end
