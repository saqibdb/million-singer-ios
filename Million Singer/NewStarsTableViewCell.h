//
//  NewStarsTableViewCell.h
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewStarsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *UserImageView;
@property (weak, nonatomic) IBOutlet UILabel *starNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *supportBtn;
@property (weak, nonatomic) IBOutlet UIButton *reciprocalBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblAims;
@property (weak, nonatomic) IBOutlet UILabel *lblEachAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblExisting;
@property (weak, nonatomic) IBOutlet UILabel *allChips;

@end
