//
//  NewStarsViewController.h
//  Million Singer
//
//  Created by MACBOOK PRO on 15/08/2017.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewStarsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)searchAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
